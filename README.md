This is a mod for Starsector to permit very large ships by combining multiple ship 'parts' together. At the moment there are two models of interaction and damage. Uniship and Multiship. A Uniship has shared health/flux, while a Multiship has independent health/flux.

The intention is to provide a common library and way of instantiating each multiship, this is also to ensure that other modders can interact with large multi-ships so that their special scripts are not broken. For instance if an onHit weapon is supposed to disable a ship, using an API the mod author should be able to detect if they have hit a multi-ship and apply the disabling effect to all parts.

There is not official released version yet and is in progress.