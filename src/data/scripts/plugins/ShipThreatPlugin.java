package data.scripts.plugins;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.input.InputEventAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

public class ShipThreatPlugin extends BaseEveryFrameCombatPlugin {

    public static Map shipMap = new HashMap();
    public static Map missileMap = new HashMap();
    public static Map shipDest = new HashMap();
    public static Map shipTarget = new HashMap();
    
    public static ArrayList getDestination(ShipAPI ship){
        if(shipTarget != null && !shipTarget.isEmpty() && shipTarget.containsKey(ship)){
            ArrayList returnList = new ArrayList();
            ShipAPI target = (ShipAPI) shipTarget.get(ship);
            returnList.add(0,target);
            return returnList;
        }  
        if(shipDest != null && !shipDest.isEmpty() && shipDest.containsKey(ship)){
            ArrayList returnList = new ArrayList();
            Vector2f dest = (Vector2f) shipDest.get(ship);
            returnList.add(0,dest);
            return returnList;
        }
        return null;
    }     
    
    public static ShipAPI getClosestThreat(ShipAPI ship){
        if(shipMap.isEmpty()) {
            return null;
        }
        if(!shipMap.containsKey(ship)) {
            return null;
        }
        
        Vector2f shipLoc = ship.getLocation();
        ShipAPI closest = null;
        float distance, closestDistance = Float.MAX_VALUE;   
        List<ShipAPI> tmpList = (List<ShipAPI>) shipMap.get(ship);
        
        for (ShipAPI tmp : tmpList)
        {
            float radiusSquared = tmp.getCollisionRadius() * tmp.getCollisionRadius();
            distance = MathUtils.getDistanceSquared(tmp.getLocation(),shipLoc) - radiusSquared;
            if (distance < closestDistance)
            {
                closest = tmp;
                closestDistance = distance;
            }
        }        
        
        return closest;
    }    
    
    public static List<ShipAPI> getThreats(ShipAPI ship){
        if(shipMap.isEmpty()) {
            return null;
        }
        if(!shipMap.containsKey(ship)) {
            return null;
        }
        return (List<ShipAPI>) shipMap.get(ship);
    }
    
    public static List<MissileAPI> getMissiles(ShipAPI ship){
        if(missileMap.isEmpty()) {
            return null;        
        }
        if(!missileMap.containsKey(ship)) {
            return null;
        }        
        return (List<MissileAPI>) missileMap.get(ship);
    }    
    private final IntervalUtil checkInterval = new IntervalUtil(0.1f,0.2f);

    @Override
    public void init(CombatEngineAPI engine) {

    }
    
    private List<ShipAPI> vacGetNearbyEnemyShips(ShipAPI ship, float range, List<ShipAPI> allShips){
        List<ShipAPI> enemies = new ArrayList<>();
        float rangesq = range * range;
        Vector2f loc = ship.getLocation();
        int owner = ship.getOwner();
        for(ShipAPI posEnemy : allShips){
            if((posEnemy.getOwner() != owner || posEnemy.isHulk()) && posEnemy.getCollisionClass() != CollisionClass.NONE && MathUtils.getDistanceSquared(loc,posEnemy.getLocation()) < rangesq){
                enemies.add(posEnemy);
            }
        }
        if(!enemies.isEmpty()) {
            return enemies;
        }
        return null;
    }
    
    private List<MissileAPI> vacGetNearbyEnemyMissiles(ShipAPI ship, float range, List<MissileAPI> allMissiles){
        List<MissileAPI> enemies = new ArrayList<>();
        float rangesq = range * range;
        Vector2f loc = ship.getLocation();
        int owner = ship.getOwner();
        for(MissileAPI posEnemy : allMissiles){
            if(posEnemy.getOwner() != owner && posEnemy.getCollisionClass() != CollisionClass.NONE && MathUtils.getDistanceSquared(loc,posEnemy.getLocation()) < rangesq){
                enemies.add(posEnemy);
            }
        }
        if(!enemies.isEmpty()) {
            return enemies;
        }
        return null;
    }    

    @Override
    public void advance(float amount,   List<InputEventAPI> events) {
        if(Global.getCombatEngine() == null) {
            return;
        }
        if (Global.getCombatEngine().isPaused()) {
            return;
        }
        
        checkInterval.advance(amount);
        if(shipMap.isEmpty() || missileMap.isEmpty() || checkInterval.intervalElapsed()){
            List<ShipAPI> allShips = Global.getCombatEngine().getShips();
            List<MissileAPI> allMissiles = Global.getCombatEngine().getMissiles();
            shipMap.clear();
            missileMap.clear();

            for(ShipAPI ship : allShips){
                List<ShipAPI> localThreats = vacGetNearbyEnemyShips(ship,5000f,allShips);
                if(localThreats != null){
                    shipMap.put(ship,localThreats);
                }
                List<MissileAPI> localMissiles = vacGetNearbyEnemyMissiles(ship,3500f,allMissiles);
                if(localMissiles != null){
                    missileMap.put(ship,localMissiles);
                }            
            }
            checkInterval.randomize();
        }
    }
}
