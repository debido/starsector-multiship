/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.twiglib.core;

/**
 *
 * @author Debio
 */
public enum DeathType {

    /**
     * The default death type, do nothing
     */
    DEFAULT,

    /**
     * Swaps the sprite to the disabled sprite ID on death.
     */
    SPRITE_SWAP,

    /**
     * On instantiation, set the origin weapon to transparent. On death, perform cleanup of deco sprites (they are reset on death by vanilla code after about 20 frames).
     */
    DECO_SWAP,

    /**
     * On death simply remove the entity altogether.
     */
    REMOVE
}
