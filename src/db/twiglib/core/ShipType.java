/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.twiglib.core;

/**
 *
 * @author Debido
 */
public enum ShipType {

    /**
     * UniChild type
     */
    UNI,
    /**
     * Simple Child node
     */
    CHILD,
    /**
     * A normal ship.
     */
    NORMAL,
    /**
     * A child host, under a root. This is not yet implemented.
     */
    CHILDHOST,
    /**
     * A root node that spawns child nodes.
     */
    ROOT
}
