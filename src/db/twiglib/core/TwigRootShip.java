/*
 * Copyright (c) 2014 Debido.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Debido - initial API and implementation and/or initial documentation
 */
package db.twiglib.core;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.mission.FleetSide;
import db.twiglib.TWIGEntity;
import db.twiglib.TwigNodeLibrary;
import org.apache.log4j.Level;

/**
 *
 * The MultiShipHost class is attached to a single weapon slot on the root/host
 * ship and is required to instantiate and spawn all the nodes.
 *
 * @author Debido
 */
public class TwigRootShip implements EveryFrameWeaponEffectPlugin {

    private static FleetSide getFleetSide(int originalOwner) {
        if (originalOwner == 100 || originalOwner == 0) {
            return FleetSide.PLAYER;

        } else {
            return FleetSide.ENEMY;
        }
    }

    private static RootNode initHost(ShipAPI hostShip) throws Exception {
        RootNode hostMultiShip = null;
        TWIGEntity mShipData;
        mShipData = TwigNodeLibrary.getRootTwigEntity(hostShip.getHullSpec().getHullId());
        //RootJSONData hostShipInit = new RootJSONData(hostShip, mShipData, fs);
        hostMultiShip = new RootNode(hostShip, mShipData);
        hostShip.setCollisionRadius(mShipData.getCombatRadius());
        float centerX = hostShip.getSpriteAPI().getCenterX();
        float centerY = hostShip.getSpriteAPI().getCenterY();

        hostShip.setSprite("multiship", mShipData.getCombatSpriteID());
        //need to do this to avoid a bug with ships that do not have a centered collision radius sprite at 0,0
        hostShip.getSpriteAPI().setCenter(centerX, centerY);

        return hostMultiShip;
    }

    private boolean fatalError = false;
    private boolean isInstanced = false;
    private int delay = 0;

    /**
     *
     * @param amount
     * @param engine
     * @param weapon
     */
    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {

        if (engine.isPaused()) {
            return;
        }
        /*
         Fix for spawn in delay/collisions
         */
        if (delay == 0) {
            delay++;
            return;
        }

        //this prevents a bug in the refit menu
        if (weapon.getShip().getOriginalOwner() == -1) {
            return;
        }

        if (fatalError) {
            return;
        }

        //if the actual Ship/Armor section is not yet instanced, let's do something about that.
        if (!isInstanced) {
            if (weapon.getShip() != null) {

                if (weapon.getLocation() != null) {

                    RootShipAPI hostShip = null;
                    try {
                        hostShip = initHost(weapon.getShip());
                    } catch (Exception ex) {
                        Global.getLogger(TwigRootShip.class).log(Level.FATAL, "Unable to spawn nodes", ex);
                        fatalError = true;
                        return;
                    }

                    if (null != hostShip) {
                        hostShip.spawnChildNodes();
                        isInstanced = true;
                    } else {
                        fatalError = true;
                    }

                }

            }

        }

    }
}
