/*
 * Copyright (c) 2014 Debido.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Debido - initial API and implementation and/or initial documentation
 */
package db.twiglib.core;

import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import db.twiglib.TWIGEntity;

/**
 *
 * @author Revan
 */
public class UniNode extends MultiShip implements UniShipAPI, MultiShipAPI {

    private float hitPointsRatio = 0f;
    private float fluxCapRatio = 0f;
    private float hitPointsLastFrame = 0f;
    private float hardFluxLastFrame = 0f;
    private float fluxDisp = 0f;

    /**
     *
     * @param hostShip
     * @param subship
     * @param weapon
     * @param mShipInit
     * @param posType
     */
    public UniNode(MultiShip hostShip, ShipAPI subship, WeaponAPI weapon, TWIGEntity mShipInit, ChildRef posType) {
        super(hostShip, subship, weapon, mShipInit);
        this.fluxDisp = subship.getMutableStats().getFluxDissipation().modified;

        if (posType == ChildRef.WEAPON) {
            setOriginType(posType);
        } else if (posType == ChildRef.VECTOR) {
            setChildOrigin(mShipInit.getLocOffset());
            setChildFacing(mShipInit.getFacing());
        } else if (posType == ChildRef.CUSTOM) {
            setOriginType(posType);
        } else {
            //panic, set to custom
            setOriginType(ChildRef.CUSTOM);
        }

    }

    /**
     * @return the hitPointsLastFrame
     */
    @Override
    public float getHitPointsLastFrame() {
        return hitPointsLastFrame;
    }

    /**
     * @param hitPointsLastFrame the hitPointsLastFrame to set
     */
    @Override
    public void setHitPointsLastFrame(float hitPointsLastFrame) {
        this.hitPointsLastFrame = hitPointsLastFrame;
    }

    /**
     * @return the hitPointsRatio
     */
    @Override
    public float getHitPointsRatio() {
        return hitPointsRatio;
    }

    /**
     * @param hitPointsRatio the hitPointsRatio to set
     */
    @Override
    public void setHitPointsRatio(float hitPointsRatio) {
        this.hitPointsRatio = hitPointsRatio;
    }

    /**
     * @return the fluxCapRatio
     */
    @Override
    public float getFluxCapRatio() {
        return fluxCapRatio;
    }

    /**
     * @param fluxCapRatio the fluxCapRatio to set
     */
    @Override
    public void setFluxCapRatio(float fluxCapRatio) {
        this.fluxCapRatio = fluxCapRatio;
    }

    void setUniRatios(float fluxCapRatio, float hitpointsRatio) {
        this.fluxCapRatio = fluxCapRatio;
        this.hitPointsRatio = hitpointsRatio;
    }

    /**
     * @return the fluxDisp
     */
    @Override
    public float getFluxDisp() {
        return fluxDisp;
    }

    /**
     * @param fluxDisp the fluxDisp to set
     */
    @Override
    public void setFluxDisp(float fluxDisp) {
        this.fluxDisp = fluxDisp;
    }

    @Override
    public float getAllUniHitpoints() {
        return ((UniShipAPI) getRootNode()).getAllUniHitpoints();
    }

    @Override
    public float getAllUniFlux() {
        return ((UniShipAPI) getRootNode()).getAllUniFlux();
    }

    /**
     * @return the hardFluxLastFrame
     */
    @Override
    public float getHardFluxLastFrame() {
        return hardFluxLastFrame;
    }

    /**
     * @param hardFluxLastFrame the hardFluxLastFrame to set
     */
    @Override
    public void setHardFluxLastFrame(float hardFluxLastFrame) {
        this.hardFluxLastFrame = hardFluxLastFrame;
    }

}
