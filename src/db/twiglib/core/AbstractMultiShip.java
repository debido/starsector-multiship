/*
 * Copyright (c) 2014 Debido.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Debido - initial API and implementation and/or initial documentation
 */
package db.twiglib.core;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.ArmorGridAPI;
import com.fs.starfarer.api.combat.BoundsAPI;
import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.FighterWingAPI;
import com.fs.starfarer.api.combat.FluxTrackerAPI;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShieldAPI;
import com.fs.starfarer.api.combat.ShipAIPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import com.fs.starfarer.api.combat.ShipEngineControllerAPI;
import com.fs.starfarer.api.combat.ShipHullSpecAPI;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipVariantAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.combat.WeaponGroupAPI;
import com.fs.starfarer.api.graphics.SpriteAPI;
import db.twiglib.TWIGEntity;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lwjgl.util.vector.Vector2f;

/**
 *
 * @author Debido
 */
public abstract class AbstractMultiShip implements MultiShipAPI {

    /**
     *
     */
    protected static final Color transparent = new Color(0, 0, 0, 0);

    /**
     *
     */
    protected static Color opaque = new Color(255, 255, 255, 255);

    /**
     * The ship object associated with this Multiship
     */
    protected ShipAPI ship = null;

    /**
     * has the node been intanced yet
     */
    protected boolean Instanced = false;

    /**
     * has the node been processed as disabled yet
     */
    protected boolean nodeDisabled = false;

    /**
     * has the node been processed as destroyed yet
     */
    protected boolean nodeDestroyed = false;

    /**
     * What type of node is it
     */
    protected ShipType multiType = ShipType.NORMAL;

    /**
     * If it is a Child Host
     */
    protected boolean ChildHost = false;

    /**
     * If it is a Root Node
     */
    protected boolean RootNode = false;

    /**
     * if it is a Child Node
     */
    protected boolean ChildNode = false;

    /**
     * the host Node for this node
     */
    protected MultiShipAPI hostNodeAPI = null;

    /**
     * The root node for this node
     */
    protected RootShipAPI rootNodeAPI = null;

    /**
     * The collision radius of the ship in the refit screen
     */
    protected float refitRadius = 0.0f;

    /**
     * The collision radius of the node during combat
     */
    protected float combatRadius = 0.0f;

    /**
     * The refit sprite ID from the settings.json graphics
     */
    protected String refitSpriteID = "";

    /**
     * The combat sprite ID from the settings.json graphics
     */
    protected String combatSpriteID = "";

    /**
     * the disabled sprite ID from the settings.json graphics
     */
    protected String disabledSpriteID = "";

    /**
     * is the status of the node retreating
     */
    protected boolean nodeRetreating = false;

    /**
     * Type of death
     */
    protected DeathType multiDeathType = DeathType.DEFAULT;

    /**
     * the list of associated nodes
     */
    protected List<MultiShip> nodeList = null;

    /**
     * has the death of the node been processed
     */
    protected boolean deathProcessed = false;

    /**
     * weapons on the host that will be disabled on death
     */
    protected List<String> onDeathHostWeaponDisable = new ArrayList<>();

    /**
     * weapons on the host ship that will be set transparent on death
     */
    protected List<String> onDeathHostWeaponAlpha = new ArrayList<>();

    /**
     * Will this node be ejected on death
     */
    protected boolean ejectOnDeath = false;

    /**
     * ejection force
     */
    protected float ejectForce = 0f;

    /**
     *
     */
    protected WeaponAPI originWeapon = null;

    /**
     *
     */
    protected boolean decoSwappedOnHostDeath = false;

    /**
     *
     */
    protected float timeAfterDeath = 0;

    /**
     * The TWIGEntity from which the ship is derived
     */
    protected TWIGEntity twigData;

    /**
     * The the child TWIG Entity data for this ship
     */
    protected List<TWIGEntity> childTwigData;
    /**
     * The method in which the origin of the child node is calculated each
     * frame, this determines how it is processed or the location update /** The
     * method in which the origin of the child node is calculated each frame,
     * this determines how it is processed or the location update
     */

    /**
     * The type of origin used by a child node
     */
    protected ChildRef originType;

    /**
     * The relative position of the child with respect to the parent
     */
    protected Vector2f childOrigin;

    /**
     * The relative angle of the child node
     */
    protected float childFacing;

    /**
     *
     */
    public AbstractMultiShip() {
    }

    /**
     * @return the decoSwappedOnHostDeath
     */
    @Override
    public boolean isDecoSwappedOnHostDeath() {
        return decoSwappedOnHostDeath;
    }

    /**
     * @param decoSwappedOnHostDeath the decoSwappedOnHostDeath to set
     */
    @Override
    public void setDecoSwappedOnHostDeath(boolean decoSwappedOnHostDeath) {
        this.decoSwappedOnHostDeath = decoSwappedOnHostDeath;
    }

    /**
     * @return the timeAfterDeath
     */
    @Override
    public float getTimeAfterDeath() {
        return timeAfterDeath;
    }

    /**
     *
     * @param amount
     */
    @Override
    public void addTimeAfterDeath(float amount) {
        timeAfterDeath += amount;
    }

    /**
     *
     * @param SpriteID
     */
    protected void swapSprite(String SpriteID) {
        float centerX = ship.getSpriteAPI().getCenterX();
        float centerY = ship.getSpriteAPI().getCenterY();
        setSprite("multiship", SpriteID);
        //need to do this to avoid a bug with ships that do not have a centered collision radius sprite at 0,0
        getSpriteAPI().setCenter(centerX, centerY);
    }

    /**
     *
     */
    @Override
    public void doDisableDamage() {
        /**
         * @param entity
         * @param point Location the damage is dealt at, in absolute engine
         * coordinates (i.e. *not* relative to the ship). MUST fall within the
         * sprite of a ship, given its current location and facing, for armor to
         * properly be taken into account.
         * @param damageAmount
         * @param damageType
         * @param empAmount
         * @param bypassShields Whether shields are ignored completely.
         * @param dealsSoftFlux Whether damage dealt to shields results in soft
         * flux.
         * @param source Should be a ShipAPI if the damage ultimately attributed
         * to it. Can also be null.
         */
        Global.getCombatEngine().applyDamage(getShip(), getShip().getLocation(), 1000000f, DamageType.KINETIC, 0f, true, true, this);
    }

    /**
     * Processes the node to be be in a 'disabled' state
     */
    @Override
    public void disable() {
        //set the multiship metadata for nodeDisabled to be true
        this.setNodeDisabled(true);
        //get the type of death.
        DeathType death = this.getMultiDeathType();
        if (death == DeathType.DEFAULT) {
        } else if (death == DeathType.SPRITE_SWAP) {
            swapSprite(getDisabledSpriteID());
        } else if (death == DeathType.DECO_SWAP) {
            decoSwap();
            setHostWeaponsInvisible();
        } else if (death == DeathType.REMOVE) {
            setNodeDestroyed(true);
            Global.getCombatEngine().removeEntity(getShip());
        }
        //if there are host weapons to disable on death, disable them
        if (!onDeathHostWeaponDisable.isEmpty()) {
            disableHostWeapons();
        }
        
        if (!onDeathHostWeaponAlpha.isEmpty()) {
            setHostWeaponsInvisible();
        }

        //if the part is ejected on death, eject it
        if (isEjectOnDeath()) {
            eject();
            getShip().getMutableStats().getMaxSpeed().unmodify("multiship_death");
        }
    }

    /**
     * On instantiation of the part, the weapon slot it was derived from is made
     * transparent
     */
    @Override
    public void decoSwap() {
        List<WeaponAPI> weapons = ((MultiShipAPI) getRootNode()).getShip().getAllWeapons();
        for (WeaponAPI w : weapons) {
            if (w.getSlot().getId().contains(getOriginWeapon().getSlot().getId())) {
                //w.getSprite().setAdditiveBlend();
                w.getSprite().setColor(transparent);
            }
        }
        this.getSpriteAPI().setColor(opaque);
    }

    /**
     * Ejects the node
     */
    @Override
    public void eject() {
        //get ejection distance
        MultiShipAPI rootShip = (MultiShipAPI) getRootNode();
        float ejectDistance = rootShip.getRefitRadius();

        //get directional vector away from host
        Vector2f ejectUnitVector = VectorUtils.getDirectionalVector(rootShip.getShip().getLocation(), getShip().getLocation());

        //begin creating ejection position
        Vector2f ejectPosition = new Vector2f(ejectUnitVector);
        ejectPosition.scale(MathUtils.getDistance(getShip().getLocation(), rootShip.getShip().getLocation()));

        //add the eject position location vector to the current location
        Vector2f.add(getShip().getLocation(), ejectPosition, ejectPosition);

        //scale the ejection force vector
        ejectUnitVector.scale(this.ejectForce);

        //apply the force
        getShip().getVelocity().set(ejectUnitVector);
        
    }

    /**
     * gets the local list of host weapons and makes them transparent
     */
    @Override
    public void setHostWeaponsInvisible() {
        List<WeaponAPI> weapons = this.getShip().getAllWeapons();
        for (WeaponAPI w : weapons) {
            for (String slotID : onDeathHostWeaponAlpha) {
                if (w.getSlot().getId().contains(slotID)) {
                    w.getSprite().setAdditiveBlend();
                    w.getSprite().setColor(transparent);
                }
            }
        }
    }

    /**
     * disables the list of host weapons
     */
    @Override
    public void disableHostWeapons() {
        List<WeaponAPI> weapons = getHostNodeAPI().getShip().getAllWeapons();
        for (WeaponAPI w : weapons) {
            for (String slotID : onDeathHostWeaponDisable) {
                if (w.getSlot().getId().contains(slotID)) {
                    w.disable(true);

                    //optionally remove the weapon from the weapon groups
                    //getHostNodeAPI().getShip().removeWeaponFromGroups(w);
                }
            }
        }
    }

    /**
     *
     * The main and most important method, this updates the location of the ship
     * part
     *
     */
    @Override
    public void updateLocation() {
        if (!isNodeDisabled() || (isNodeDisabled() && !isEjectOnDeath())) {
            if (getOriginType() == ChildRef.WEAPON) {
                //Set the location of the ship part to the position of the origin weapon
                getShip().getLocation().set(originWeapon.getLocation());
                //get the direction the root node is facing, then re-orient the ship part to the same facing direction
                getShip().setFacing(getHostNodeAPI().getShip().getFacing() + getOriginWeapon().getSlot().getAngle());
                getShip().getVelocity().set(getHostNodeAPI().getShip().getVelocity());
            } else if (getOriginType() == ChildRef.VECTOR) {
                Vector2f position = getChildOrigin();
                float facing = getChildFacing();
                CombatEntityAPI root = ((CombatEntityAPI) getRootNode());
                
                Vector2f updatePosition = new Vector2f(root.getLocation());
                Vector2f.add(updatePosition, position, updatePosition);
                
                getShip().getLocation().set(updatePosition);
                getShip().setFacing(root.getFacing() + facing);
                
            } else {
                /**
                 * The origin type would have to be custom, in which case
                 * nothing is done on custom. That is in the purview of content
                 * creators.
                 */
            }
            
        }
    }

    /**
     * @return the originWeapon
     */
    @Override
    public WeaponAPI getOriginWeapon() {
        return originWeapon;
    }

    /**
     * @param originWeapon the originWeapon to set
     */
    @Override
    public void setOriginWeapon(WeaponAPI originWeapon) {
        this.originWeapon = originWeapon;
    }

    /**
     * @return the nodeList
     */
    @Override
    public List<MultiShipAPI> getNodeList() {
        return MultiShipManager.getGlobalMultiShipAPIs().get(getRootNode());
    }

    /**
     * @return the ship
     */
    @Override
    public ShipAPI getShip() {
        return ship;
    }

    /**
     * @param ship the ship to set
     */
    @Override
    public void setShip(ShipAPI ship) {
        this.ship = ship;
    }

    /**
     * @return the Alive
     */
    @Override
    public boolean isNodeAlive() {
        return !nodeDisabled;
    }

    /**
     * @return the Instanced
     */
    @Override
    public boolean isInstanced() {
        return Instanced;
    }

    /**
     * @param Instanced the Instanced to set
     */
    @Override
    public void setInstanced(boolean Instanced) {
        this.Instanced = Instanced;
    }

    /**
     * @return the UniType
     */
    @Override
    public boolean isUniType() {
        return multiType == ShipType.UNI;
    }

    /**
     */
    @Override
    public void setUniType() {
        this.multiType = ShipType.UNI;
    }

    /**
     * @return the SubType
     */
    @Override
    public boolean isSubType() {
        return multiType == ShipType.CHILD;
    }

    /**
     */
    @Override
    public void setSubType() {
        this.multiType = ShipType.CHILD;
    }

    /**
     * @return the multiType
     */
    @Override
    public ShipType getMultiType() {
        return multiType;
    }

    /**
     * @param multiType the multiType to set
     */
    @Override
    public void setMultiType(ShipType multiType) {
        this.multiType = multiType;
    }

    /**
     * @return the ChildHost
     */
    @Override
    public boolean isChildHost() {
        return ChildHost;
    }

    /**
     * @param ChildHost the ChildHost to set
     */
    @Override
    public void setChildHost(boolean ChildHost) {
        this.ChildHost = ChildHost;
    }

    /**
     * @return the RootNode
     */
    @Override
    public boolean isRootNode() {
        return RootNode;
    }

    /**
     * @param RootNode the RootNode to set
     */
    @Override
    public void setIsRootNode(boolean RootNode) {
        this.RootNode = RootNode;
    }

    /**
     * @return the ChildNode
     */
    @Override
    public boolean isChildNode() {
        return ChildNode;
    }

    /**
     * @param ChildNode the ChildNode to set
     */
    @Override
    public void setChildNode(boolean ChildNode) {
        this.ChildNode = ChildNode;
    }

    /**
     * @return the hostNodeAPI
     */
    @Override
    public MultiShipAPI getHostNode() {
        return getHostNodeAPI();
    }

    /**
     * @param hostNode the hostNodeAPI to set
     */
    @Override
    public void setHostNode(MultiShipAPI hostNode) {
        this.setHostNodeAPI(hostNode);
    }

    /**
     * @return the rootNodeAPI
     */
    @Override
    public RootShipAPI getRootNode() {
        return rootNodeAPI;
    }

    /**
     * @param rootNode the rootNodeAPI to set
     */
    @Override
    public void setRootNode(RootShipAPI rootNode) {
        this.rootNodeAPI = rootNode;
    }

    /**
     * @return the nodeDestroyed
     */
    @Override
    public boolean isNodeDestroyed() {
        return nodeDestroyed;
    }

    /**
     * @param nodeDestroyed the nodeDestroyed to set
     */
    @Override
    public void setNodeDestroyed(boolean nodeDestroyed) {
        this.nodeDestroyed = nodeDestroyed;
    }

    /**
     * @return the nodeDisabled
     */
    @Override
    public boolean isNodeDisabled() {
        return nodeDisabled;
    }

    /**
     * @param nodeDisabled the nodeDisabled to set
     */
    @Override
    public void setNodeDisabled(boolean nodeDisabled) {
        this.nodeDisabled = nodeDisabled;
    }

    /**
     * @return the refitRadius
     */
    @Override
    public float getRefitRadius() {
        return refitRadius;
    }

    /**
     * @return the combatRadius
     */
    @Override
    public float getCombatRadius() {
        return combatRadius;
    }

    /**
     * @return the refitSpriteID
     */
    @Override
    public String getRefitSpriteID() {
        return refitSpriteID;
    }

    /**
     * @return the combatSpriteID
     */
    @Override
    public String getCombatSpriteID() {
        return combatSpriteID;
    }

    /**
     * @return the disabledSpriteID
     */
    @Override
    public String getDisabledSpriteID() {
        return disabledSpriteID;
    }

    /**
     * @return the nodeRetreating
     */
    @Override
    public boolean isNodeRetreating() {
        return nodeRetreating;
    }

    /**
     * @param nodeRetreating the nodeRetreating to set
     */
    @Override
    public void setNodeRetreating(boolean nodeRetreating) {
        this.nodeRetreating = nodeRetreating;
    }

    /**
     * @return the multiDeathType
     */
    @Override
    public DeathType getMultiDeathType() {
        return multiDeathType;
    }

    /**
     * @param multiDeathType the multiDeathType to set
     */
    @Override
    public void setMultiDeathType(DeathType multiDeathType) {
        this.multiDeathType = multiDeathType;
    }

    /**
     * @return the deathProcessed
     */
    @Override
    public boolean isDeathProcessed() {
        return deathProcessed;
    }

    /**
     * @param deathProcessed the deathProcessed to set
     */
    @Override
    public void setDeathProcessed(boolean deathProcessed) {
        this.deathProcessed = deathProcessed;
    }

    /**
     * @return the hostNodeAPI
     */
    protected MultiShipAPI getHostNodeAPI() {
        return hostNodeAPI;
    }

    /**
     * @param hostNodeAPI the hostNodeAPI to set
     */
    protected void setHostNodeAPI(MultiShipAPI hostNodeAPI) {
        this.hostNodeAPI = hostNodeAPI;
    }

    //SS Default Helper methods, allows usage of this MultiShip in a ShipAPI list
    @Override
    public String getFleetMemberId() {
        return getShip().getFleetMemberId();
    }
    
    @Override
    public Vector2f getMouseTarget() {
        return getShip().getMouseTarget();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isShuttlePod() {
        return getShip().isShuttlePod();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isDrone() {
        return getShip().isDrone();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isFighter() {
        return getShip().isFighter();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isFrigate() {
        return getShip().isFrigate();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isDestroyer() {
        return getShip().isDestroyer();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isCruiser() {
        return getShip().isCruiser();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isCapital() {
        return getShip().isCapital();
    }

    /**
     *
     * @return
     */
    @Override
    public HullSize getHullSize() {
        return getShip().getHullSize();
    }

    /**
     *
     * @return
     */
    @Override
    public ShipAPI getShipTarget() {
        return getShip().getShipTarget();
    }

    /**
     *
     * @param ship
     */
    @Override
    public void setShipTarget(ShipAPI ship) {
        getShip().setShipTarget(ship);
    }
    
    @Override
    public int getOriginalOwner() {
        return getShip().getOriginalOwner();
    }

    /**
     *
     * @param originalOwner
     */
    @Override
    public void setOriginalOwner(int originalOwner) {
        getShip().setOriginalOwner(originalOwner);
    }

    /**
     *
     */
    @Override
    public void resetOriginalOwner() {
        getShip().resetOriginalOwner();
    }

    /**
     *
     * @return
     */
    @Override
    public MutableShipStatsAPI getMutableStats() {
        return getShip().getMutableStats();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isHulk() {
        return getShip().isHulk();
    }

    /**
     *
     * @return
     */
    @Override
    public List<WeaponAPI> getAllWeapons() {
        return getShip().getAllWeapons();
    }

    /**
     *
     * @return
     */
    @Override
    public ShipSystemAPI getPhaseCloak() {
        return getShip().getPhaseCloak();
    }

    /**
     *
     * @return
     */
    @Override
    public ShipSystemAPI getSystem() {
        return getShip().getSystem();
    }

    /**
     *
     * @return
     */
    @Override
    public ShipSystemAPI getTravelDrive() {
        return getShip().getTravelDrive();
    }

    /**
     *
     */
    @Override
    public void toggleTravelDrive() {
        getShip().toggleTravelDrive();
    }

    /**
     *
     * @param type
     * @param shieldUpkeep
     * @param shieldEfficiency
     * @param arc
     */
    @Override
    public void setShield(ShieldAPI.ShieldType type, float shieldUpkeep, float shieldEfficiency, float arc) {
        getShip().setShield(type, shieldUpkeep, shieldEfficiency, arc);
    }

    /**
     *
     * @return
     */
    @Override
    public ShipHullSpecAPI getHullSpec() {
        return getShip().getHullSpec();
    }

    /**
     *
     * @return
     */
    @Override
    public ShipVariantAPI getVariant() {
        return getShip().getVariant();
    }
    
    @Override
    public void useSystem() {
        getShip().useSystem();
    }

    /**
     *
     * @return
     */
    @Override
    public FluxTrackerAPI getFluxTracker() {
        return getShip().getFluxTracker();
    }
    
    @Override
    @Deprecated
    public List<ShipAPI> getWingMembers() {
        return getShip().getWingMembers();
    }
    
    @Override
    @Deprecated
    public ShipAPI getWingLeader() {
        return getShip().getWingLeader();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isWingLeader() {
        return getShip().isWingLeader();
    }

    /**
     *
     * @return
     */
    @Override
    public FighterWingAPI getWing() {
        return getShip().getWing();
    }

    /**
     *
     * @return
     */
    @Override
    public List<ShipAPI> getDeployedDrones() {
        return getShip().getDeployedDrones();
    }

    /**
     *
     * @return
     */
    @Override
    public ShipAPI getDroneSource() {
        return getShip().getDroneSource();
    }
    
    @Override
    public Object getWingToken() {
        return getShip().getWingToken();
    }

    /**
     *
     * @return
     */
    @Override
    public ArmorGridAPI getArmorGrid() {
        return getShip().getArmorGrid();
    }

    /**
     *
     * @param renderBounds
     */
    @Override
    public void setRenderBounds(boolean renderBounds) {
        getShip().setRenderBounds(renderBounds);
    }

    /**
     *
     * @param cr
     */
    @Override
    public void setCRAtDeployment(float cr) {
        getShip().setCRAtDeployment(cr);
    }

    /**
     *
     * @return
     */
    @Override
    public float getCRAtDeployment() {
        return getShip().getCRAtDeployment();
    }

    /**
     *
     * @return
     */
    @Override
    public float getCurrentCR() {
        return getShip().getCurrentCR();
    }

    /**
     *
     * @param cr
     */
    @Override
    public void setCurrentCR(float cr) {
        getShip().setCurrentCR(cr);
    }

    /**
     *
     * @return
     */
    @Override
    public float getWingCRAtDeployment() {
        return getShip().getWingCRAtDeployment();
    }

    /**
     *
     * @return
     */
    @Override
    public float getRemainingWingCR() {
        return getShip().getRemainingWingCR();
    }

    /**
     *
     * @param value
     */
    @Override
    public void setHitpoints(float value) {
        getShip().setHitpoints(value);
    }
    
    @Override
    public float getTimeDeployedForCRReduction() {
        return getShip().getTimeDeployedForCRReduction();
    }

    /**
     *
     * @return
     */
    @Override
    public float getFullTimeDeployed() {
        return getShip().getFullTimeDeployed();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean losesCRDuringCombat() {
        return getShip().losesCRDuringCombat();
    }
    
    @Override
    public boolean controlsLocked() {
        return getShip().controlsLocked();
    }

    /**
     *
     * @param controlsLocked
     */
    @Override
    public void setControlsLocked(boolean controlsLocked) {
        getShip().setControlsLocked(controlsLocked);
    }

    /**
     *
     * @param systemDisabled
     */
    @Override
    public void setShipSystemDisabled(boolean systemDisabled) {
        getShip().setShipSystemDisabled(systemDisabled);
    }
    
    @Override
    public Set<WeaponAPI> getDisabledWeapons() {
        return getShip().getDisabledWeapons();
    }
    
    @Override
    public int getNumFlameouts() {
        return getShip().getNumFlameouts();
    }

    /**
     *
     * @return
     */
    @Override
    public float getHullLevelAtDeployment() {
        return getShip().getHullLevelAtDeployment();
    }
    
    @Override
    public void setSprite(String category, String key) {
        getShip().setSprite(category, key);
    }
    
    @Override
    public SpriteAPI getSpriteAPI() {
        return getShip().getSpriteAPI();
    }

    /**
     *
     * @return
     */
    @Override
    public ShipEngineControllerAPI getEngineController() {
        return getShip().getEngineController();
    }
    
    @Override
    public void giveCommand(ShipCommand command, Object param, int groupNumber) {
        getShip().giveCommand(command, param, groupNumber);
    }
    
    @Override
    public void setShipAI(ShipAIPlugin ai) {
        getShip().setShipAI(ai);
    }
    
    @Override
    public ShipAIPlugin getShipAI() {
        return getShip().getShipAI();
    }
    
    @Override
    public void resetDefaultAI() {
        getShip().resetDefaultAI();
    }

    /**
     *
     */
    @Override
    public void turnOnTravelDrive() {
        getShip().turnOnTravelDrive();
    }

    /**
     *
     * @param dur
     */
    @Override
    public void turnOnTravelDrive(float dur) {
        getShip().turnOnTravelDrive(dur);
    }

    /**
     *
     */
    @Override
    public void turnOffTravelDrive() {
        getShip().turnOffTravelDrive();
    }
    
    @Override
    public void abortLanding() {
        getShip().abortLanding();
    }
    
    @Override
    public void beginLandingAnimation(ShipAPI target) {
        getShip().beginLandingAnimation(target);
    }
    
    @Override
    public boolean isLanding() {
        return getShip().isLanding();
    }
    
    @Override
    public boolean isFinishedLanding() {
        return getShip().isFinishedLanding();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isInsideNebula() {
        return getShip().isInsideNebula();
    }

    /**
     *
     * @param isInsideNebula
     */
    @Override
    public void setInsideNebula(boolean isInsideNebula) {
        getShip().setInsideNebula(isInsideNebula);
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isAffectedByNebula() {
        return getShip().isAffectedByNebula();
    }

    /**
     *
     * @param affectedByNebula
     */
    @Override
    public void setAffectedByNebula(boolean affectedByNebula) {
        getShip().setAffectedByNebula(affectedByNebula);
    }
    
    @Override
    public float getDeployCost() {
        return getShip().getDeployCost();
    }
    
    @Override
    public void removeWeaponFromGroups(WeaponAPI weapon) {
        getShip().removeWeaponFromGroups(weapon);
    }
    
    @Override
    public void applyCriticalMalfunction(Object module) {
        getShip().applyCriticalMalfunction(module);
    }

    /**
     *
     * @return
     */
    @Override
    public float getBaseCriticalMalfunctionDamage() {
        return getShip().getBaseCriticalMalfunctionDamage();
    }

    /**
     *
     * @return
     */
    @Override
    public float getEngineFractionPermanentlyDisabled() {
        return getShip().getEngineFractionPermanentlyDisabled();
    }
    
    @Override
    public float getCombinedAlphaMult() {
        return getShip().getCombinedAlphaMult();
    }

    /**
     *
     * @return
     */
    @Override
    public float getLowestHullLevelReached() {
        return getShip().getLowestHullLevelReached();
    }
    
    @Override
    public ShipwideAIFlags getAIFlags() {
        return getShip().getAIFlags();
    }

    /**
     *
     * @return
     */
    @Override
    public List<WeaponGroupAPI> getWeaponGroupsCopy() {
        return getShip().getWeaponGroupsCopy();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isHoldFire() {
        return getShip().isHoldFire();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isHoldFireOneFrame() {
        return getShip().isHoldFireOneFrame();
    }

    /**
     *
     * @param holdFireOneFrame
     */
    @Override
    public void setHoldFireOneFrame(boolean holdFireOneFrame) {
        getShip().setHoldFireOneFrame(holdFireOneFrame);
    }

    /**
     *
     * @param source
     * @param color
     * @param intensity
     * @param copies
     * @param range
     */
    @Override
    public void setJitter(Object source, Color color, float intensity, int copies, float range) {
        getShip().setJitter(source, color, intensity, copies, range);
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isPhased() {
        return getShip().isPhased();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isRetreating() {
        return getShip().isRetreating();
    }
    
    @Override
    public void setRetreating(boolean retreating) {
        getShip().setRetreating(retreating);
    }
    
    @Override
    public boolean isAlive() {
        return getShip().isAlive();
    }

    /**
     *
     * @return
     */
    @Override
    public Vector2f getLocation() {
        return getShip().getLocation();
    }

    /**
     *
     * @return
     */
    @Override
    public Vector2f getVelocity() {
        return getShip().getLocation();
    }

    /**
     *
     * @return
     */
    @Override
    public float getFacing() {
        return getShip().getFacing();
    }

    /**
     *
     * @param facing
     */
    @Override
    public void setFacing(float facing) {
        getShip().setFacing(facing);
    }

    /**
     *
     * @return
     */
    @Override
    public float getAngularVelocity() {
        return getShip().getAngularVelocity();
    }

    /**
     *
     * @param angVel
     */
    @Override
    public void setAngularVelocity(float angVel) {
        getShip().setAngularVelocity(angVel);
    }

    /**
     *
     * @return
     */
    @Override
    public int getOwner() {
        return getShip().getOwner();
    }

    /**
     *
     * @param owner
     */
    @Override
    public void setOwner(int owner) {
        getShip().setOwner(owner);
    }

    /**
     *
     * @return
     */
    @Override
    public float getCollisionRadius() {
        return getShip().getCollisionRadius();
    }

    /**
     *
     * @return
     */
    @Override
    public CollisionClass getCollisionClass() {
        return getShip().getCollisionClass();
    }

    /**
     *
     * @param collisionClass
     */
    @Override
    public void setCollisionClass(CollisionClass collisionClass) {
        getShip().setCollisionClass(collisionClass);
    }

    /**
     *
     * @return
     */
    @Override
    public float getMass() {
        return getShip().getMass();
    }

    /**
     *
     * @param mass
     */
    @Override
    public void setMass(float mass) {
        getShip().setMass(mass);
    }
    
    @Override
    public BoundsAPI getExactBounds() {
        return getShip().getExactBounds();
    }
    
    @Override
    public ShieldAPI getShield() {
        return getShip().getShield();
    }
    
    @Override
    public float getHullLevel() {
        return getShip().getHullLevel();
    }
    
    @Override
    public float getHitpoints() {
        return getShip().getHitpoints();
    }
    
    @Override
    public float getMaxHitpoints() {
        return getShip().getMaxHitpoints();
    }
    
    @Override
    public void setCollisionRadius(float radius) {
        getShip().setCollisionRadius(radius);
    }

    /**
     * @return the twigData
     */
    @Override
    public TWIGEntity getTwigData() {
        return twigData;
    }

    /**
     * @param twigData the twigData to set
     */
    @Override
    public void setTwigData(TWIGEntity twigData) {
        this.twigData = twigData;
    }

    /**
     * @return the childTwigData
     */
    @Override
    public List<TWIGEntity> getChildTwigData() {
        return Collections.unmodifiableList(childTwigData);
    }

    /**
     * @param childTwigData the childTwigData to set
     */
    @Override
    public void setChildTwigData(List<TWIGEntity> childTwigData) {
        this.childTwigData = childTwigData;
    }

    /**
     * @return the originType
     */
    @Override
    public ChildRef getOriginType() {
        return originType;
    }

    /**
     * @param originType the originType to set
     */
    @Override
    public void setOriginType(ChildRef originType) {
        this.originType = originType;
    }

    /**
     * @return the childOrigin
     */
    public Vector2f getChildOrigin() {
        return childOrigin;
    }

    /**
     * @param childOrigin the childOrigin to set
     */
    public void setChildOrigin(Vector2f childOrigin) {
        this.childOrigin = childOrigin;
    }

    /**
     * @return the childFacing
     */
    public float getChildFacing() {
        return childFacing;
    }

    /**
     * @param childFacing the childFacing to set
     */
    public void setChildFacing(float childFacing) {
        this.childFacing = childFacing;
    }
    
    @Override
    public boolean isAlly() {
        return getShip().isAlly();
    }
    
    @Override
    public void setWeaponGlow(float f, Color color, EnumSet<WeaponAPI.WeaponType> es) {
        getShip().setWeaponGlow(f, color, es);
    }
    
    @Override
    public void setVentCoreColor(Color color) {
        getShip().setVentCoreColor(color);
    }
    
    /**
     *
     * @param color
     */
    @Override
    public void setVentFringeColor(Color color) {
        getShip().setVentFringeColor(color);
    }
    
    @Override
    public Color getVentCoreColor() {
        return getShip().getVentCoreColor();
    }
    
    @Override
    public Color getVentFringeColor() {
        return getShip().getVentFringeColor();
    }
    
    @Override
    public void setVentCoreTexture(String string) {
        getShip().setVentCoreTexture(string);
    }
    
    @Override
    public void setVentFringeTexture(String string) {
        getShip().setVentFringeTexture(string);
    }
    
    @Override
    public String getVentFringeTexture() {
        return getShip().getVentFringeTexture();
    }
    
    @Override
    public String getVentCoreTexture() {
        return getShip().getVentCoreTexture();
    }
    
    @Override
    public String getHullStyleId() {
        return getShip().getHullStyleId();
    }
    
    @Override
    public WeaponGroupAPI getWeaponGroupFor(WeaponAPI wapi) {
        return getShip().getWeaponGroupFor(wapi);
    }
    
    @Override
    public void setCopyLocation(Vector2f vctrf, float f, float f1) {
        getShip().setCopyLocation(vctrf, f, f1);
    }
    
    @Override
    public void setAlly(boolean ally) {
        getShip().setAlly(ally);
    }

    @Override
    public void applyCriticalMalfunction(Object module, boolean permanent) {
        getShip().applyCriticalMalfunction(module, permanent);
    }

    @Override
    public String getId() {
        return getShip().getId();
    }

    @Override
    public String getName() {
        return getShip().getName();
    }

    @Override
    public void setJitterUnder(Object source, Color color, float intensity, int copies, float range) {
        getShip().setJitterUnder(source, color, intensity, copies, range);
    }

    @Override
    public void setJitter(Object source, Color color, float intensity, int copies, float minRange, float range) {
        getShip().setJitter(source, color, intensity, copies, minRange, range);
    }

    @Override
    public void setJitterUnder(Object source, Color color, float intensity, int copies, float minRange, float range) {
        getShip().setJitterUnder(source, color, intensity, copies, minRange, range);
    }

    @Override
    public float getTimeDeployedUnderPlayerControl() {
        return getShip().getTimeDeployedUnderPlayerControl();
    }

    @Override
    public SpriteAPI getSmallTurretCover() {
        return getShip().getSmallTurretCover();
    }

    @Override
    public SpriteAPI getSmallHardpointCover() {
        return getShip().getSmallHardpointCover();
    }

    @Override
    public SpriteAPI getMediumTurretCover() {
        return getShip().getMediumTurretCover();
    }

    @Override
    public SpriteAPI getMediumHardpointCover() {
        return getShip().getMediumHardpointCover();
    }

    @Override
    public SpriteAPI getLargeTurretCover() {
        return getShip().getLargeTurretCover();
    }

    @Override
    public SpriteAPI getLargeHardpointCover() {
        return getShip().getLargeHardpointCover();
    }

    @Override
    public boolean isDefenseDisabled() {
        return getShip().isDefenseDisabled();
    }

    @Override
    public void setDefenseDisabled(boolean defenseDisabled) {
        getShip().setDefenseDisabled(defenseDisabled);
    }

    @Override
    public void setPhased(boolean phased) {
        getShip().setPhased(phased);
    }

    @Override
    public void setExtraAlphaMult(float transparency) {
        getShip().setExtraAlphaMult(transparency);
    }

    @Override
    public void setApplyExtraAlphaToEngines(boolean applyExtraAlphaToEngines) {
        getShip().setApplyExtraAlphaToEngines(applyExtraAlphaToEngines);
    }

    @Override
    public void setOverloadColor(Color color) {
        getShip().setOverloadColor(color);
    }

    @Override
    public void resetOverloadColor() {
        getShip().resetOverloadColor();
    }

    @Override
    public Color getOverloadColor() {
        return getShip().getOverloadColor();
    }

    @Override
    public boolean isRecentlyShotByPlayer() {
        return getShip().isRecentlyShotByPlayer();
    }

    @Override
    public float getMaxSpeedWithoutBoost() {
        return getShip().getMaxSpeedWithoutBoost();
    }

    @Override
    public float getHardFluxLevel() {
        return getShip().getHardFluxLevel();
    }

    @Override
    public void fadeToColor(Object source, Color color, float durIn, float durOut, float maxShift) {
        getShip().fadeToColor(source, color, durIn, durOut, maxShift);
    }

    @Override
    public boolean isShowModuleJitterUnder() {
        return getShip().isShowModuleJitterUnder();
    }

    @Override
    public void setShowModuleJitterUnder(boolean showModuleJitterUnder) {
        getShip().setShowModuleJitterUnder(showModuleJitterUnder);
    }

    @Override
    public void addAfterimage(Color color, float locX, float locY, float velX, float velY, float maxJitter, float in, float dur, float out, boolean additive, boolean combineWithSpriteColor, boolean aboveShip) {
        getShip().addAfterimage(color, locX, locY, velX, velY, maxJitter, in, dur, out, additive, combineWithSpriteColor, aboveShip);
    }
    
    @Override
    public boolean isEjectOnDeath(){
        return ejectOnDeath;
    }
}

    

