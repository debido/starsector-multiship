/*
 * Copyright (c) 2014 Debido.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Debido - initial API and implementation and/or initial documentation
 */
package db.twiglib.core;

/**
 *
 * @author Debido
 */
public interface UniShipAPI {

    /**
     * @return the fluxCapRatio
     */
    float getFluxCapRatio();

    /**
     * @return the fluxDisp
     */
    float getFluxDisp();

    /**
     * @return the hitPointsLastFrame
     */
    float getHitPointsLastFrame();

    /**
     * @return the hitPointsRatio
     */
    float getHitPointsRatio();

    /**
     * @param fluxCapRatio the fluxCapRatio to set
     */
    void setFluxCapRatio(float fluxCapRatio);

    /**
     * @param fluxDisp the fluxDisp to set
     */
    void setFluxDisp(float fluxDisp);

    /**
     * @param hitPointsLastFrame the hitPointsLastFrame to set
     */
    void setHitPointsLastFrame(float hitPointsLastFrame);

    /**
     * @param hitPointsRatio the hitPointsRatio to set
     */
    void setHitPointsRatio(float hitPointsRatio);

    /**
     * @return gets the sum of all hitpoints for the uniShip
     */
    float getAllUniHitpoints();

    /**
     * @return the hitPointsRatio of the entire uniship
     */
    float getAllUniFlux();

    /**
     * @return the hardFluxLastFrame
     */
    float getHardFluxLastFrame();

    /**
     * @param hardFluxLastFrame the hardFluxLastFrame to set
     */
    void setHardFluxLastFrame(float hardFluxLastFrame);

}
