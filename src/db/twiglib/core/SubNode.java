/*
 * Copyright (c) 2014 Debido.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Debido - initial API and implementation and/or initial documentation
 */
package db.twiglib.core;

import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import db.twiglib.TWIGEntity;

/**
 *
 * @author Revan
 */
public class SubNode extends MultiShip implements MultiShipAPI {
    

    /**
     *
     * @param hostShip
     * @param subship
     * @param weapon
     * @param mShipInit
     * @param posType
     */
    public SubNode(MultiShip hostShip, ShipAPI subship, WeaponAPI weapon, TWIGEntity mShipInit, ChildRef posType) {
        super(hostShip, subship, weapon, mShipInit);
        
        if (posType == ChildRef.WEAPON) {
            setOriginType(posType);
        } else if (posType == ChildRef.VECTOR) {
            setChildOrigin(mShipInit.getLocOffset());
            setChildFacing(mShipInit.getFacing());
        } else if (posType == ChildRef.CUSTOM) {
            setOriginType(posType);
        } else {
            //panic, set to custom
            setOriginType(ChildRef.CUSTOM);
        }

    }

}
