/*
 * Copyright (c) 2014 Debido.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Debido - initial API and implementation and/or initial documentation
 */
package db.twiglib.core;

import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import db.twiglib.TWIGEntity;

/**
 *
 * @author Debido
 */
public class TWIGFactory {

    public static MultiShipAPI createUniChild(RootNode aThis, ShipAPI subship, WeaponAPI w, TWIGEntity mShipData, ChildRef posType) {
        MultiShipAPI multiship = new UniNode(aThis, subship, w, mShipData, posType);
        
        return multiship;
    }

    public static MultiShipAPI createChildNode(RootNode aThis, ShipAPI subship, WeaponAPI w, TWIGEntity mShipData, ChildRef posType) {
        MultiShipAPI multiship = new SubNode(aThis, subship, w, mShipData, posType);
        return multiship;
    }
    
    /**
     *
     * @param hostShip
     * @param initData
     * @return
     */
    public RootShipAPI createRootShip(ShipAPI hostShip, TWIGEntity initData) {
        RootShipAPI root = new RootNode(hostShip, initData);
        
        return root;

    }
    
    
}
