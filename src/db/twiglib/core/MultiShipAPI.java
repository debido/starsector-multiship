/*
 * Copyright (c) 2014 Debido.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Debido - initial API and implementation and/or initial documentation
 */
package db.twiglib.core;

import com.fs.starfarer.api.combat.ArmorGridAPI;
import com.fs.starfarer.api.combat.BoundsAPI;
import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.FighterWingAPI;
import com.fs.starfarer.api.combat.FluxTrackerAPI;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShieldAPI;
import com.fs.starfarer.api.combat.ShipAIPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import com.fs.starfarer.api.combat.ShipEngineControllerAPI;
import com.fs.starfarer.api.combat.ShipHullSpecAPI;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipVariantAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.combat.WeaponGroupAPI;
import com.fs.starfarer.api.graphics.SpriteAPI;
import db.twiglib.TWIGEntity;
import java.awt.Color;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import org.lwjgl.util.vector.Vector2f;

/**
 *
 * @author Debido
 */
public interface MultiShipAPI extends ShipAPI {

    void abortLanding();

    /**
     *
     */
    void addTimeAfterDeath(float amount);

    void applyCriticalMalfunction(Object module);

    void beginLandingAnimation(ShipAPI target);

    boolean controlsLocked();

    /**
     *
     */
    void decoSwap();

    /**
     *
     */
    void disable();

    /**
     *
     */
    void disableHostWeapons();

    /**
     *
     */
    void doDisableDamage();

    /**
     *
     */
    void eject();

    ShipwideAIFlags getAIFlags();

    /**
     *
     * @return
     */
    List<WeaponAPI> getAllWeapons();

    /**
     *
     * @return
     */
    float getAngularVelocity();

    /**
     *
     * @return
     */
    ArmorGridAPI getArmorGrid();

    /**
     *
     * @return
     */
    float getBaseCriticalMalfunctionDamage();

    /**
     *
     * @return
     */
    float getCRAtDeployment();

    /**
     *
     * @return
     */
    CollisionClass getCollisionClass();

    /**
     *
     * @return
     */
    float getCollisionRadius();

    /**
     * @return the combatRadius
     */
    float getCombatRadius();

    /**
     * @return the combatSpriteID
     */
    String getCombatSpriteID();

    float getCombinedAlphaMult();

    /**
     *
     * @return the current CR
     */
    float getCurrentCR();

    float getDeployCost();

    /**
     *
     * @return
     */
    List<ShipAPI> getDeployedDrones();

    /**
     * @return the disabledSpriteID
     */
    String getDisabledSpriteID();

    Set<WeaponAPI> getDisabledWeapons();

    /**
     *
     * @return
     */
    ShipAPI getDroneSource();

    /**
     *
     * @return
     */
    ShipEngineControllerAPI getEngineController();

    /**
     *
     * @return
     */
    float getEngineFractionPermanentlyDisabled();

    BoundsAPI getExactBounds();

    /**
     *
     * @return
     */
    float getFacing();

    //SS Default Helper methods, allows usage of this MultiShip in a ShipAPI list
    String getFleetMemberId();

    /**
     *
     * @return
     */
    FluxTrackerAPI getFluxTracker();

    /**
     * @return the framesAfterDeath
     */
    float getTimeAfterDeath();

    /**
     *
     * @return
     */
    float getFullTimeDeployed();

    float getHitpoints();

    /**
     * @return the hostNodeAPI
     */
    MultiShipAPI getHostNode();

    float getHullLevel();

    /**
     *
     * @return
     */
    float getHullLevelAtDeployment();

    /**
     *
     * @return
     */
    HullSize getHullSize();

    /**
     *
     * @return
     */
    ShipHullSpecAPI getHullSpec();

    /**
     *
     * @return
     */
    Vector2f getLocation();

    /**
     *
     * @return
     */
    float getLowestHullLevelReached();

    /**
     *
     * @return
     */
    float getMass();

    float getMaxHitpoints();

    Vector2f getMouseTarget();

    /**
     * @return the multiDeathType
     */
    DeathType getMultiDeathType();

    /**
     * @return the multiType
     */
    ShipType getMultiType();

    /**
     *
     * @return
     */
    MutableShipStatsAPI getMutableStats();

    /**
     * @return the nodeList
     */
    List<MultiShipAPI> getNodeList();

    int getNumFlameouts();

    /**
     * @return the originWeapon
     */
    WeaponAPI getOriginWeapon();

    int getOriginalOwner();

    /**
     *
     * @return
     */
    int getOwner();

    /**
     *
     * @return
     */
    ShipSystemAPI getPhaseCloak();

    /**
     * @return the refitRadius
     */
    float getRefitRadius();

    /**
     * @return the refitSpriteID
     */
    String getRefitSpriteID();

    /**
     *
     * @return
     */
    float getRemainingWingCR();

    /**
     * @return the rootNodeAPI
     */
    RootShipAPI getRootNode();

    ShieldAPI getShield();

    /**
     * @return the ship
     */
    ShipAPI getShip();

    ShipAIPlugin getShipAI();

    /**
     *
     * @return
     */
    ShipAPI getShipTarget();

    SpriteAPI getSpriteAPI();

    /**
     *
     * @return
     */
    ShipSystemAPI getSystem();

    float getTimeDeployedForCRReduction();

    /**
     *
     * @return
     */
    ShipSystemAPI getTravelDrive();

    /**
     *
     * @return
     */
    ShipVariantAPI getVariant();

    /**
     *
     * @return
     */
    Vector2f getVelocity();

    /**
     *
     * @return
     */
    List<WeaponGroupAPI> getWeaponGroupsCopy();

    /**
     *
     * @return
     */
    FighterWingAPI getWing();

    /**
     *
     * @return
     */
    float getWingCRAtDeployment();

    ShipAPI getWingLeader();

    List<ShipAPI> getWingMembers();

    Object getWingToken();

    void giveCommand(ShipCommand command, Object param, int groupNumber);

    /**
     *
     * @return
     */
    boolean isAffectedByNebula();

    boolean isAlive();

    /**
     *
     * @return
     */
    boolean isCapital();

    /**
     * @return the ChildHost
     */
    boolean isChildHost();

    /**
     * @return the ChildNode
     */
    boolean isChildNode();

    /**
     *
     * @return
     */
    boolean isCruiser();

    /**
     * @return the deathProcessed
     */
    boolean isDeathProcessed();

    /**
     * @return the decoSwappedOnHostDeath
     */
    boolean isDecoSwappedOnHostDeath();

    /**
     *
     * @return
     */
    boolean isDestroyer();

    /**
     *
     * @return
     */
    boolean isDrone();

    /**
     *
     * @return
     */
    boolean isFighter();

    boolean isFinishedLanding();

    /**
     *
     * @return
     */
    boolean isFrigate();

    /**
     *
     * @return
     */
    boolean isHoldFire();

    /**
     *
     * @return
     */
    boolean isHoldFireOneFrame();

    /**
     *
     * @return
     */
    boolean isHulk();

    /**
     *
     * @return
     */
    boolean isInsideNebula();

    /**
     * @return the Instanced
     */
    boolean isInstanced();

    boolean isLanding();

    /**
     * @return the Alive
     */
    boolean isNodeAlive();

    /**
     * @return the nodeDestroyed
     */
    boolean isNodeDestroyed();

    /**
     * @return the nodeDisabled
     */
    boolean isNodeDisabled();

    /**
     * @return the nodeRetreating
     */
    boolean isNodeRetreating();

    /**
     *
     * @return
     */
    boolean isPhased();

    /**
     *
     * @return
     */
    boolean isRetreating();

    /**
     * @return the RootNode
     */
    boolean isRootNode();

    /**
     *
     * @return
     */
    boolean isShuttlePod();

    /**
     * @return the SubType
     */
    boolean isSubType();

    /**
     * @return the UniType
     */
    boolean isUniType();

    /**
     *
     * @return
     */
    boolean isWingLeader();

    /**
     *
     * @return
     */
    boolean losesCRDuringCombat();

    void removeWeaponFromGroups(WeaponAPI weapon);

    void resetDefaultAI();

    /**
     *
     */
    void resetOriginalOwner();

    /**
     *
     * @param affectedByNebula
     */
    void setAffectedByNebula(boolean affectedByNebula);

    /**
     *
     * @param angVel
     */
    void setAngularVelocity(float angVel);

    /**
     *
     * @param cr
     */
    void setCRAtDeployment(float cr);

    /**
     * @param ChildHost the ChildHost to set
     */
    void setChildHost(boolean ChildHost);

    /**
     * @param ChildNode the ChildNode to set
     */
    void setChildNode(boolean ChildNode);

    /**
     *
     * @param collisionClass
     */
    void setCollisionClass(CollisionClass collisionClass);

    void setCollisionRadius(float radius);

    /**
     *
     * @param controlsLocked
     */
    void setControlsLocked(boolean controlsLocked);

    /**
     *
     * @param cr
     */
    void setCurrentCR(float cr);

    /**
     * @param deathProcessed the deathProcessed to set
     */
    void setDeathProcessed(boolean deathProcessed);

    /**
     * @param decoSwappedOnHostDeath the decoSwappedOnHostDeath to set
     */
    void setDecoSwappedOnHostDeath(boolean decoSwappedOnHostDeath);

    /**
     *
     * @param facing
     */
    void setFacing(float facing);

    /**
     *
     * @param value
     */
    void setHitpoints(float value);

    /**
     *
     * @param holdFireOneFrame
     */
    void setHoldFireOneFrame(boolean holdFireOneFrame);

    /**
     * @param hostNode the hostNodeAPI to set
     */
    void setHostNode(MultiShipAPI hostNode);

    /**
     *
     */
    void setHostWeaponsInvisible();

    /**
     *
     * @param isInsideNebula
     */
    void setInsideNebula(boolean isInsideNebula);

    /**
     * @param Instanced the Instanced to set
     */
    void setInstanced(boolean Instanced);

    /**
     * @param RootNode the RootNode to set
     */
    void setIsRootNode(boolean RootNode);

    /**
     *
     * @param source
     * @param color
     * @param intensity
     * @param copies
     * @param range
     */
    void setJitter(Object source, Color color, float intensity, int copies, float range);

    /**
     *
     * @param mass
     */
    void setMass(float mass);

    /**
     * @param multiDeathType the multiDeathType to set
     */
    void setMultiDeathType(DeathType multiDeathType);

    /**
     * @param multiType the multiType to set
     */
    void setMultiType(ShipType multiType);

    /**
     * @param nodeDestroyed the nodeDestroyed to set
     */
    void setNodeDestroyed(boolean nodeDestroyed);

    /**
     * @param nodeDisabled the nodeDisabled to set
     */
    void setNodeDisabled(boolean nodeDisabled);

    /**
     * @param nodeRetreating the nodeRetreating to set
     */
    void setNodeRetreating(boolean nodeRetreating);

    /**
     * @param originWeapon the originWeapon to set
     */
    void setOriginWeapon(WeaponAPI originWeapon);

    /**
     *
     * @param originalOwner
     */
    void setOriginalOwner(int originalOwner);

    /**
     *
     * @param owner
     */
    void setOwner(int owner);

    /**
     *
     * @param renderBounds
     */
    void setRenderBounds(boolean renderBounds);

    void setRetreating(boolean retreating);

    /**
     * @param rootNode the rootNodeAPI to set
     */
    void setRootNode(RootShipAPI rootNode);

    /**
     *
     * @param type
     * @param shieldUpkeep
     * @param shieldEfficiency
     * @param arc
     */
    void setShield(ShieldAPI.ShieldType type, float shieldUpkeep, float shieldEfficiency, float arc);

    /**
     * @param ship the ship to set
     */
    void setShip(ShipAPI ship);

    void setShipAI(ShipAIPlugin ai);

    /**
     *
     * @param systemDisabled
     */
    void setShipSystemDisabled(boolean systemDisabled);

    /**
     *
     * @param ship
     */
    void setShipTarget(ShipAPI ship);

    void setSprite(String category, String key);

    /**
     */
    void setSubType();

    /**
     */
    void setUniType();

    /**
     *
     */
    void toggleTravelDrive();

    /**
     *
     */
    void turnOffTravelDrive();

    /**
     *
     */
    void turnOnTravelDrive();

    /**
     *
     * @param dur
     */
    void turnOnTravelDrive(float dur);

    /**
     *
     */
    void updateLocation();

    void useSystem();

    /**
     * @return the twigData
     */
    public TWIGEntity getTwigData();

    /**
     * @param twigData the twigData to set
     */
    public void setTwigData(TWIGEntity twigData);

    /**
     * @return the childTwigData
     */
    public List<TWIGEntity> getChildTwigData();

    /**
     * @param childTwigData the childTwigData to set
     */
    public void setChildTwigData(List<TWIGEntity> childTwigData);

    /**
     * @return the originType
     */
    public ChildRef getOriginType();

    /**
     * @param originType the originType to set
     */
    public void setOriginType(ChildRef originType);

    public boolean isAlly();

    public void setWeaponGlow(float f, Color color, EnumSet<WeaponAPI.WeaponType> es);

    public void setVentCoreColor(Color color);

    public void setVentFringeColor(Color color);

    public Color getVentCoreColor();

    public Color getVentFringeColor();

    public void setVentCoreTexture(String string);

    public void setVentFringeTexture(String string);

    public String getVentFringeTexture();

    public String getVentCoreTexture();

    public String getHullStyleId();

    public WeaponGroupAPI getWeaponGroupFor(WeaponAPI wapi);

    public void setCopyLocation(Vector2f vctrf, float f, float f1);
    
    public boolean isEjectOnDeath();
}
