/*
 * Copyright (c) 2014 Debido.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Debido - initial API and implementation and/or initial documentation
 */
package db.twiglib.core;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.EngineSlotAPI;
import com.fs.starfarer.api.combat.FighterWingAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import com.fs.starfarer.api.combat.ShipEngineControllerAPI;
import com.fs.starfarer.api.combat.ShipEngineControllerAPI.ShipEngineAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.util.ValueShifterAPI;
import db.data.scripts.shipai.No_AI;
import db.data.scripts.shipai.Slave_AI;
import db.twiglib.TWIGEntity;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Level;
import org.lwjgl.util.vector.Vector2f;

/**
 *
 * @author Revan
 */
public class RootNode extends MultiShip implements UniShipAPI, RootShipAPI, MultiShipAPI {

    private float allUniHitpoints = 0f;
    private float allUniFlux = 0f;
    private float hitPointsRatio = 0f;
    private float fluxCapRatio = 0f;
    private float hitPointsLastFrame = 0f;
    private float fluxDisp = 0f;
    private float hardFluxLastFrame;
    public ShipEngineControllerAPI rootEngineController;

    /**
     *
     * The default constructor for creating a RootNode class.
     *
     * @param hostShip
     * @param mShipData
     */
    public RootNode(ShipAPI hostShip, TWIGEntity mShipData) {
        super(hostShip, mShipData);

        this.rootEngineController = new RootEngine(this);
        this.childTwigData = mShipData.getConnectedShips();
    }

    @Override
    public void spawnChildNodes() {
        addSegments(this.getChildTwigData());
    }

    /**
     *
     * Add child nodes/segments to the rootnode
     *
     * @param segmentData
     * @return
     */
    @Override
    public List<MultiShipAPI> addSegments(List<TWIGEntity> segmentData) {
        //create a list empty list of multiships
        List<MultiShipAPI> mShips = new ArrayList<>();
        List<MultiShipAPI> uShips = new ArrayList<>();

        //initialise each sub component and add it to the mShips list
        for (TWIGEntity mShipData : segmentData) {
            if (mShipData.getShipType().contentEquals("CHILD")) {
                try {
                    mShips.add(addChildSegment(mShipData));
                } catch (Exception ex) {
                    Global.getLogger(RootNode.class).log(Level.FATAL, "Error, unable to spawn: " + mShipData.getShipID(), ex);
                }
            } else if (mShipData.getShipType().contentEquals("UNI")) {
                try {
                    uShips.add(addUniSegment(mShipData));
                } catch (Exception ex) {
                    Global.getLogger(RootNode.class).log(Level.FATAL, "Error, unable to spawn: " + mShipData.getShipID(), ex);
                }
            } else if (mShipData.getShipType().contentEquals("CHILDHOST")) {
                throw new UnsupportedOperationException("CHILDHOST Not supported yet."); //not supported yet
            } else if (mShipData.getShipType().contentEquals("ROOT")) {
                throw new UnsupportedOperationException("A ROOT cannot spawn another ROOT"); //not supported yet
            } else if (mShipData.getShipType().contentEquals("NORMAL")) {
                throw new UnsupportedOperationException("NORMAL not a normal spawnable operation"); //not supported yet
            } else {
                throw new UnsupportedOperationException("Unknown ShipType"); //not supported yet
            }

        }

        if (!uShips.isEmpty()) {
            initUniShip(uShips);
            for (MultiShipAPI uShip : uShips) {
                mShips.add(uShip);
            }
        }

        updateGlobalList(mShips);

        setNodeAI(mShips);

        //register this new MultiShip set host with subcomponents in the GlobalSubShipObjects for later retrieval
        //MultiShipManager.getGlobalMultiships().put(hostShip, mShips);
        //finally add the list of multiships we created as an object list
        return mShips;
    }

    /**
     *
     * Add a single child node
     *
     * @param mShipData
     * @return
     * @throws java.lang.Exception
     */
    public MultiShipAPI addChildSegment(TWIGEntity mShipData) throws Exception {
        ChildRef posType = null;
        FleetSide fs = this.getFleetSide();
        String variant = mShipData.getVariantID();
        Vector2f spawnLocation = null;
        WeaponAPI w = null;
        if (!mShipData.getWeaponID().contentEquals("")) {
            List<WeaponAPI> ws = this.getShip().getAllWeapons();
            for (WeaponAPI weapon : ws) {
                if (weapon.getSlot().getId().contentEquals(mShipData.getWeaponID())) {
                    w = weapon;
                    spawnLocation = weapon.getLocation();
                    posType = ChildRef.WEAPON;
                }
            }
        } else if (mShipData.getLocOffset() != null && !(mShipData.getLocOffset().x == 0 && mShipData.getLocOffset().y == 0)) {
            spawnLocation = mShipData.getLocOffset();
            Vector2f.add(spawnLocation, this.getShip().getLocation(), spawnLocation);
            posType = ChildRef.VECTOR;
        } else {
            /**
             * This is a bit dangerous, spawning at 0,0, the intention should be
             * for whoever is doing this to have overriden the root node class
             * and is doing something special with the location possibly using
             * the additional properties
             */
            posType = ChildRef.CUSTOM;
            spawnLocation = new Vector2f(0, 0);
        }

        //spawn the actual subship
        ShipAPI subship = Global.getCombatEngine().getFleetManager(fs).spawnShipOrWing(variant, spawnLocation, this.getShip().getFacing());
        if (this.isAlly()) {
            Global.getCombatEngine().getFleetManager(fs).getDeployedFleetMember(this).getMember().setAlly(true);
        }
        subship.getMutableStats().getMaxSpeed().modifyFlat("multiship_death", -600f);
        float centerX = subship.getSpriteAPI().getCenterX();
        float centerY = subship.getSpriteAPI().getCenterY();
        subship.setSprite("multiship", mShipData.getCombatSpriteID());
        //need to do this to avoid a bug with ships that do not have a centered collision radius sprite at 0,0
        subship.getSpriteAPI().setCenter(centerX, centerY);

        //create a multiship data object
        //MultiShip mShip = new SubNode(hostShip, subship, w, mShipData);
        MultiShipAPI mShip = TWIGFactory.createChildNode(this, subship, w, mShipData, posType);

        return mShip;
    }

    /**
     *
     * Add a UniChild type segment
     *
     * @param mShipData
     * @return
     * @throws java.lang.Exception
     */
    public MultiShipAPI addUniSegment(TWIGEntity mShipData) throws Exception {

        ChildRef posType = null;
        FleetSide fs = this.getFleetSide();
        String variant = mShipData.getVariantID();
        Vector2f spawnLocation = null;
        WeaponAPI w = null;
        if (!mShipData.getWeaponID().contentEquals("")) {
            List<WeaponAPI> ws = this.getShip().getAllWeapons();
            for (WeaponAPI weapon : ws) {
                if (weapon.getSlot().getId().contentEquals(mShipData.getWeaponID())) {
                    w = weapon;
                    spawnLocation = weapon.getLocation();
                    posType = ChildRef.WEAPON;
                }
            }
        } else if (!(mShipData.getLocOffset().x == 0 && mShipData.getLocOffset().y == 0)) {
            spawnLocation = mShipData.getLocOffset();
            posType = ChildRef.VECTOR;
        } else {
            /**
             * This is a bit dangerous, spawning at 0,0, the intention should be
             * for whoever is doing this to have overriden the root node class
             * and is doing something special with the location possibly using
             * the additional properties
             */
            posType = ChildRef.CUSTOM;
            spawnLocation = new Vector2f(0, 0);
        }

        /**
         * Suppress ship spawning messages but allow them in dev mode.
         */
        
        boolean didSuppress = false;
        
        if(!Global.getSettings().isDevMode() && !Global.getCombatEngine().getFleetManager(fs).isSuppressDeploymentMessages()){
            Global.getCombatEngine().getFleetManager(fs).setSuppressDeploymentMessages(true);
            didSuppress = true;
        }
        
        //spawn the actual subship
        ShipAPI subship = Global.getCombatEngine().getFleetManager(fs).spawnShipOrWing(variant, spawnLocation, this.getShip().getFacing());
        
        /**
         * Re-enable suppression again to previous state.
         */
        if (didSuppress){
            Global.getCombatEngine().getFleetManager(fs).setSuppressDeploymentMessages(false);
        }
        if (this.getShip().isAlly()) {
            subship.setAlly(true);
        }
        subship.getMutableStats().getMaxSpeed().modifyFlat("multiship_death", -600f);
        float centerX = subship.getSpriteAPI().getCenterX();
        float centerY = subship.getSpriteAPI().getCenterY();
        subship.setSprite("multiship", mShipData.getCombatSpriteID());
        //need to do this to avoid a bug with ships that do not have a centered collision radius sprite at 0,0
        subship.getSpriteAPI().setCenter(centerX, centerY);

        //create a multiship data object
        //MultiShip mShip = new SubNode(hostShip, subship, w, mShipData);
        MultiShipAPI mShip = TWIGFactory.createUniChild(this, subship, w, mShipData, posType);

        return mShip;
    }

    /**
     *
     * The default method used by the manager process to tell it to update all
     * child nodes
     *
     * @param intervalElapsed
     * @param amount
     */
    @Override
    public void updateNodes(boolean intervalElapsed, float amount) {

        //for uni components
        uniDistribute(amount);

        //check deaths.
        checkDeaths(amount);

        //runs the update locations for all sub nodes
        setNodeLocations(amount);

        //check if retreating is occurring
        checkRetreating(amount);

    }

    /**
     *
     * Tells the root node to check if it or any of it's child nodes are
     * dead/disabled and process them accordingly
     *
     * @param amount
     */
    public void checkDeaths(float amount) {
        /*
         This method is for detecting the death(s) of the root node or it's CHILD/UNICHILD components and then handling with the death
         as is specified in the multiship_entities.json file
         */

        //if the hostShip is destroyed, we assume that all it's subnodes have been disabled/destroyed as well, so iterate the next host
        if (this.isNodeDestroyed()) {
            return;
        }

        cleanFighters();

        //if the host ship is disabled, then check if it is vaporised
        if (this.isNodeDisabled()) {

            for (MultiShipAPI node : getNodeList()) {
                if (node.getMultiDeathType() == DeathType.DECO_SWAP
                        && !node.isDecoSwappedOnHostDeath()
                        && node.isNodeDisabled()) {
                    if (node.getTimeAfterDeath() > 0.5f) {
                        node.decoSwap();
                        node.setDecoSwappedOnHostDeath(true);
                    } else {
                        node.addTimeAfterDeath(amount);
                    }

                }
            }

            //if the host ship is no longer in play, then we assume the ship is fully so let's vaporise other uni parts
            if (!Global.getCombatEngine().isEntityInPlay(this.getShip())) {
                for (MultiShipAPI part : getUniNodes()) {
                    Global.getCombatEngine().removeEntity(part.getShip());
                    part.setNodeDestroyed(true);
                }

                this.setNodeDestroyed(true);
            }

            //we're done here, returning to main thread
            return;

        }

        //checks if the host ship or uniship component object is actually a hulk
        boolean aNodeIsDisabled = false;
        for (MultiShipAPI p : getAllLocalNodes()) {
            if (p.getShip().isHulk() && p.isNodeAlive()) {
                aNodeIsDisabled = true;
                break;
            }
        }

        if (aNodeIsDisabled) {
            if (this.getShip().isHulk()) {
                for (MultiShipAPI node : getNodeList()) {
                    if (node.getShip().isAlive() && !node.isNodeDisabled()) {
                        node.doDisableDamage();
                        node.disable();
                    } else {
                        node.disable();
                    }
                }
                this.setNodeDisabled(true);
            } //if a uni Node is disabled
            else if (isUniNodeDisabled()) {
                for (MultiShipAPI p : getUniNodes()) {
                    if (p.getShip().isHulk() && !p.isNodeDisabled()) {
                        p.disable();
                        MultiShipAPI rootShip = (MultiShipAPI) p.getRootNode();
                        if (rootShip.getHitpoints() <= 1f && rootShip.isAlive()) {
                            rootShip.doDisableDamage();
                        }

                    }
                }

                //a sub node should have been disabled...but let's check for safety
            } else if (isSubNodeDisabled()) {
                for (MultiShipAPI p : getChildNodes()) {
                    if (p.getShip().isHulk() && !p.isNodeDisabled()) {
                        p.disable();
                    }
                }
            }

        }
    }

    public void cleanFighters() {
        for (MultiShipAPI node : getNodeList()) {

            if (node.getShip().isFighter()) {
                FighterWingAPI wing = node.getShip().getWing();
                List<ShipAPI> wingMembers = wing.getWingMembers();
                for (ShipAPI wingMember : wingMembers) {
                    if (wingMember.isAlive() && !node.isAlive()) {
                        Global.getCombatEngine().removeEntity(wingMember);
                    }
                }
            }

        }
    }

    /**
     * Checks if a UniChild node is disabled
     *
     * @return
     */
    public boolean isUniNodeDisabled() {
        for (MultiShipAPI uniNode : getUniNodes()) {
            if (uniNode.getShip().isHulk()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if a Child Node is disabled
     *
     * @return
     */
    public boolean isSubNodeDisabled() {
        for (MultiShipAPI subNode : getChildNodes()) {
            if (subNode.getShip().isHulk()) {
                return true;
            }
        }

        return false;
    }

    /**
     * A method to get all the child nodes + the root node in a single array
     * ready to use
     *
     * @return
     */
    public List<MultiShipAPI> getAllLocalNodes() {
        List<MultiShipAPI> allParts = new ArrayList<>(getNodeList());
        allParts.add(this);
        return allParts;
    }

    /**
     *
     * For each node in the node list, update their location
     *
     * @param amount
     */
    public void setNodeLocations(float amount) {
        //update node locations
        for (MultiShipAPI node : getNodeList()) {
            node.updateLocation();
        }
    }

    /**
     * Distributes all the flux/health resources amongst the root and UniChild
     * nodes This method is for redistributing health/flux/hardflux amongst the
     * root and UNICHILD components. The math and setup is fairly complex But
     * basicaly it's like this:
     *
     * 1. On instantation of the root and components the value of all of the
     * flux/hitpoint values and calculates the fraction it contributes
     *
     * 2. Based upon the contribution the hitpoints/flux a damage reduction buff
     * will be applied
     *
     * 3. During distribution the reduction buff and difference between the
     * health flux in the last frame is compared to the current frame
     *
     * 4. The difference between the frames is multiplied by the inverse of the
     * contribution ratio and that total added/removed to the pool of
     * flux/hitpoints.
     *
     * 5. Flux/hitpoints redistributed to all member nodes of the UNIFIED ship
     * entity
     *
     * Why so complicated? Mainly to avoid high alpha strike damage from
     * something like a reaper torp taking out a component, causing a cascade
     * failure.
     *
     * @param amount
     */
    public void uniDistribute(float amount) {

        List<MultiShipAPI> uniParts = getAllUniShipParts();

        //if there are no uniParts, do nothing
        if (uniParts.isEmpty()) {
            return;
        }

        //add the hostship for averaging all the flux information
        uniParts.add(this);

        //Step 2. Redistribute all the health/flux amongst parts
        float maxFlux = getAllUniFlux();
        float sumAllFlux = 0f;
        float sumAllHardFlux = 0f;
        float maxHealth = getAllUniHitpoints();
        float sumHealth = 0f;

        float additionalFlux = 0f;
        float additionalHit = 0f;

        //get max and cumulative flux and hit points
        for (MultiShipAPI part : uniParts) {
            float contributionRatio = ((UniShipAPI) part).getHitPointsRatio();

            //maxFlux += part.getShip().getFluxTracker().getMaxFlux();
            sumAllFlux += part.getFluxTracker().getCurrFlux();
            sumAllHardFlux += part.getFluxTracker().getHardFlux();

            //get the hitpoins this frame
            float health = part.getShip().getHitpoints();

            sumHealth += health;

            float healthLastFrame = ((UniShipAPI) part).getHitPointsLastFrame();

            if (health < healthLastFrame) {
                float healthDelta = healthLastFrame - health;
                float additionalHitAmount = healthDelta / contributionRatio;
                additionalHit += additionalHitAmount;
            }

            if (null == part.getShield()) {
                continue;
            }
            //check if the shield is on
            boolean shieldOn = part.getShield().isOn();

            //get the difference between the hard flux in this frame and the last frame
            float deltaHFlux = part.getFluxTracker().getHardFlux() - ((UniShipAPI) part).getHardFluxLastFrame();

            //get the amount of hard flux that could have been vented in the frame
            float hFluxShunted = part.getMutableStats().getHardFluxDissipationFraction().getModifiedValue() * part.getFluxTracker().getMaxFlux();

            if (shieldOn) {
                if (deltaHFlux + hFluxShunted > 0f) {
                    float foo = deltaHFlux + hFluxShunted;
                    float ratio = ((UniShipAPI) part).getHitPointsRatio();
                    additionalFlux += (deltaHFlux + hFluxShunted) / contributionRatio;
                }
            }

        }

        sumAllHardFlux += additionalFlux;
        sumHealth -= additionalHit;

        if (sumHealth <= 0f) {
            return;
        }

        //redistribute
        for (MultiShipAPI part : uniParts) {
            float healthFraction = part.getShip().getMaxHitpoints() / maxHealth;
            float fluxFraction = part.getShip().getFluxTracker().getMaxFlux() / maxFlux;
            part.getFluxTracker().setCurrFlux(sumAllFlux * fluxFraction);
            part.getFluxTracker().setHardFlux(sumAllHardFlux * fluxFraction);

            //reset the hard flux for the last frame to the current frame
            ((UniShipAPI) part).setHardFluxLastFrame(sumAllHardFlux * fluxFraction);
            //reset the hitpoints last frame to the current frame
            ((UniShipAPI) part).setHitPointsLastFrame(sumHealth * healthFraction);

            part.setHitpoints(sumHealth * healthFraction);
        }

        if (MultiShipManager.getVentTracker().intervalElapsed()) {
            if (this.getShip().getFluxTracker().isVenting()) {
                for (MultiShipAPI part : uniParts) {

                    //if it's the host ship...skip
                    if (part == this.getShip()) {
                        continue;
                    }
                    part.giveCommand(ShipCommand.VENT_FLUX, null, 0);
                }
            }
        }

    }

    /**
     * In this method we check if roots are retreating, then set states and if
     * necessary dispose of retreating parts that aren't far enough off the map.
     *
     * @param amount
     */
    public void checkRetreating(float amount) {

        //if the host ship is retreating, set the other parts to be retreating for pickup at the map edge.
        if (!this.isNodeRetreating() && this.getShip().isRetreating()) {
            this.setNodeRetreating(true);

            for (MultiShipAPI part : getNodeList()) {
                part.getShip().setRetreating(true);
                part.setNodeRetreating(true);
            }
        }
        //if the host ship has retreated, some parts may have left, but others may not, just remove the other entities from play automatically
        if (this.isNodeRetreating() && !Global.getCombatEngine().isEntityInPlay(this.getShip())) {
            for (MultiShipAPI part : getNodeList()) {
                if (Global.getCombatEngine().isEntityInPlay(part.getShip())) {
                    //Global.getCombatEngine().removeEntity(part.getShip());

                    /*Before we removed the entity, but a better way is to let the combat engine pick it up.
                     Normally there is an issue with the ship not getting far enough off the map for the ship to be picked up.
                     In this case we're getting the height of the combat map
                     dividing it by two
                     and if the owner of the ship is 0 (the player) we make the number negative as the
                     map values / Vector2f coordinate values are between +/- half the height of the map.
                    
                     After we have the valid retreat location, we set the position of the part as 0f (x), the (+/-) mapheight with an extra thousand to ensure pickup
                     */
                    float mapHeight = Global.getCombatEngine().getMapHeight();
                    mapHeight /= 2f;
                    mapHeight += 1000f;

                    if (this.getShip().getOwner() == 0) {
                        mapHeight *= -1f;
                    }

                    part.getLocation().set(0f, mapHeight);
                }
            }
        }

    }

    public List<MultiShipAPI> getChildNodes() {
        List<MultiShipAPI> subNodes = getNodeList();

        for (Iterator<MultiShipAPI> it = subNodes.iterator(); it.hasNext();) {
            MultiShipAPI node = it.next();
            if (node.isUniType()) {
                it.remove();
            }
        }

        return subNodes;
    }

    public List<MultiShipAPI> getUniNodes() {
        List<MultiShipAPI> subNodes = new ArrayList<>(getNodeList());

        for (Iterator<MultiShipAPI> it = subNodes.iterator(); it.hasNext();) {
            MultiShipAPI node = it.next();
            if (node.isSubType()) {
                it.remove();
            }
        }

        return subNodes;
    }

    public List<MultiShipAPI> getAllUniShipParts() {
        List<MultiShipAPI> uniParts = new ArrayList<>(getUniNodes());

        return uniParts;
    }

    /**
     * @param nodeList the nodeList to set
     */
    @Override
    public void updateGlobalList(List<MultiShipAPI> nodeList) {
        MultiShipManager.registerRoot(this, nodeList);

        //register the host ship string name and all the subcomponent string names.
        MultiShipManager.registerNode(this.getShip(), this);

        for (MultiShipAPI node : nodeList) {
            MultiShipManager.registerNode(node.getShip(), node);
        }

    }

    /**
     *
     * @param uShips
     */
    public void initUniShip(List<MultiShipAPI> uShips) {
        //float sumFluxDisp = getShip().getMutableStats().getFluxDissipation().modified;
        float sumFluxCapacity = getShip().getFluxTracker().getMaxFlux();
        float sumHitpoints = getShip().getMaxHitpoints();

        for (MultiShipAPI uShip : uShips) {
            //for each, add their capacity to the sum
            sumFluxCapacity += uShip.getShip().getFluxTracker().getMaxFlux();
            sumHitpoints += uShip.getShip().getMaxHitpoints();
        }

        for (MultiShipAPI uShip : uShips) {
            //get the component capacity/hitpoints
            float fluxCapacity = uShip.getShip().getFluxTracker().getMaxFlux();
            float hitpoints = uShip.getShip().getMaxHitpoints();

            //divide it by the sum
            float fluxRatio = fluxCapacity / sumFluxCapacity;
            float hitpointsRatio = hitpoints / sumHitpoints;

            //apply shield/hull damage take as a fraction
            uShip.getMutableStats().getShieldDamageTakenMult().modifyMult("uniShip_shield_init", fluxRatio);
            uShip.getMutableStats().getHullDamageTakenMult().modifyMult("uniShip_hull_init", hitpointsRatio);

            //((UniShipAPI)uShip).setUniRatios(fluxRatio, hitpointsRatio);
            ((UniShipAPI) uShip).setFluxCapRatio(fluxRatio);
            ((UniShipAPI) uShip).setHitPointsRatio(hitpointsRatio);
            ((UniShipAPI) uShip).setHitPointsLastFrame(uShip.getHitpoints());

        }

        //get the component capacity/hitpoints for the root
        float fluxCapacity = getShip().getFluxTracker().getMaxFlux();
        float hitpoints = getShip().getMaxHitpoints();

        //divide it by the sum
        float fluxRatio = fluxCapacity / sumFluxCapacity;
        float hitpointsRatio = hitpoints / sumHitpoints;

        //apply to the root
        getMutableStats().getShieldDamageTakenMult().modifyMult("uniShip_shield_init", fluxRatio);
        getMutableStats().getHullDamageTakenMult().modifyMult("uniShip_hull_init", hitpointsRatio);

        setUniRatios(fluxRatio, hitpointsRatio);
        setHitPointsLastFrame(getShip().getHitpoints());

        setAllUniFlux(sumFluxCapacity);
        setAllUniHitpoints(sumHitpoints);
        setHitPointsLastFrame(getShip().getHitpoints());

    }

    /**
     * @return the allUniHitpoints
     */
    @Override
    public float getAllUniHitpoints() {
        return allUniHitpoints;
    }

    /**
     * @param allUniHitpoints the allUniHitpoints to set
     */
    public void setAllUniHitpoints(float allUniHitpoints) {
        this.allUniHitpoints = allUniHitpoints;
    }

    /**
     * @return the allUniFlux
     */
    @Override
    public float getAllUniFlux() {
        return allUniFlux;
    }

    /**
     * @param allUniFlux the allUniFlux to set
     */
    public void setAllUniFlux(float allUniFlux) {
        this.allUniFlux = allUniFlux;
    }

    /**
     * @return the hitPointsRatio
     */
    @Override
    public float getHitPointsRatio() {
        return hitPointsRatio;
    }

    /**
     * @param hitPointsRatio the hitPointsRatio to set
     */
    @Override
    public void setHitPointsRatio(float hitPointsRatio) {
        this.hitPointsRatio = hitPointsRatio;
    }

    /**
     * @return the fluxCapRatio
     */
    @Override
    public float getFluxCapRatio() {
        return fluxCapRatio;
    }

    /**
     * @param fluxCapRatio the fluxCapRatio to set
     */
    @Override
    public void setFluxCapRatio(float fluxCapRatio) {
        this.fluxCapRatio = fluxCapRatio;
    }

    /**
     * @return the hitPointsLastFrame
     */
    @Override
    public float getHitPointsLastFrame() {
        return hitPointsLastFrame;
    }

    /**
     * @param hitPointsLastFrame the hitPointsLastFrame to set
     */
    @Override
    public void setHitPointsLastFrame(float hitPointsLastFrame) {
        this.hitPointsLastFrame = hitPointsLastFrame;
    }

    /**
     * @return the fluxDisp
     */
    @Override
    public float getFluxDisp() {
        return fluxDisp;
    }

    /**
     * @param fluxDisp the fluxDisp to set
     */
    @Override
    public void setFluxDisp(float fluxDisp) {
        this.fluxDisp = fluxDisp;
    }

    /**
     * @return the hardFluxLastFrame
     */
    @Override
    public float getHardFluxLastFrame() {
        return hardFluxLastFrame;
    }

    /**
     * @param hardFluxLastFrame the hardFluxLastFrame to set
     */
    @Override
    public void setHardFluxLastFrame(float hardFluxLastFrame) {
        this.hardFluxLastFrame = hardFluxLastFrame;
    }

    /**
     * A quick helper method for setting both UniChild ratios for flux and
     * hitpoints
     *
     * @param fluxRatio
     * @param hitpointsRatio
     */
    protected void setUniRatios(float fluxRatio, float hitpointsRatio) {
        this.fluxCapRatio = fluxRatio;
        this.hitPointsRatio = hitpointsRatio;
    }

    /**
     * @return the nodeList
     */
    @Override
    public List<MultiShipAPI> getNodeList() {
        return MultiShipManager.getNodesFromRoot(this);
    }

    /**
     *
     * Returns the mass of all attached ship parts plus the root node itself.
     *
     * @return
     */
    @Override
    public float getMass() {
        float mass = getShip().getMass();
        for (MultiShip node : nodeList) {
            if (node.isAlive()) {
                mass += node.getMass();
            } else if (node.isHulk() && !node.ejectOnDeath) {
                mass += node.getMass();
            }
        }

        return mass;
    }

    /**
     * returns the refit radius of the root node, which should be the total
     * radius of the entire composite ship
     *
     * @return
     */
    @Override
    public float getCollisionRadius() {
        return this.getRefitRadius();
    }

    /**
     *
     * Returns all weapons across all nodes including the root
     *
     * @return
     */
    @Override
    public List<WeaponAPI> getAllNodeWeapons() {
        List<WeaponAPI> allWeapons = new ArrayList<>();

        allWeapons.addAll(this.getShip().getAllWeapons());

        for (MultiShipAPI mShip : getNodeList()) {
            allWeapons.addAll(mShip.getAllWeapons());
        }

        return allWeapons;

    }

    /**
     * Returns all disabled weapons across all nodes including the root
     *
     * @return
     */
    @Override
    public List<WeaponAPI> getAllNodeDisabledWeapons() {
        List<WeaponAPI> allWeapons = new ArrayList<>();

        for (MultiShipAPI mShip : getAllLocalNodes()) {
            for (WeaponAPI shipweapon : mShip.getAllWeapons()) {
                if (shipweapon.isDisabled()) {
                    allWeapons.add(shipweapon);
                }
            }
        }

        return allWeapons;

    }

    /**
     *
     * Returns all engines across all nodes
     *
     * @return
     */
    @Override
    public List<ShipEngineControllerAPI.ShipEngineAPI> getAllNodeShipEngines() {
        List<ShipEngineControllerAPI.ShipEngineAPI> allEngines = new ArrayList<>();
        for (MultiShipAPI mShip : getAllLocalNodes()) {
            allEngines.addAll(mShip.getEngineController().getShipEngines());
        }

        return allEngines;

    }

    /**
     * Returns all disabled ship engines across all nodes including the root
     *
     * @return
     */
    @Override
    public List<ShipEngineControllerAPI.ShipEngineAPI> getAllNodeDisabledShipEngines() {
        List<ShipEngineControllerAPI.ShipEngineAPI> allEngines = new ArrayList<>();

        for (MultiShipAPI mShip : getAllLocalNodes()) {
            for (ShipEngineAPI shipengine : mShip.getEngineController().getShipEngines()) {
                if (shipengine.isDisabled()) {
                    allEngines.add(shipengine);
                }
            }
        }

        return allEngines;

    }

    @Override
    public ShipEngineControllerAPI getEngineController() {
        return getRootEngineController();
    }

    @Override
    public List<WeaponAPI> getAllWeapons() {
        return getAllNodeWeapons();
    }

    /**
     * @return the rootEngineController
     */
    public ShipEngineControllerAPI getRootEngineController() {
        return rootEngineController;
    }

    /**
     * @param rootEngineController the rootEngineController to set
     */
    public void setRootEngineController(ShipEngineControllerAPI rootEngineController) {
        this.rootEngineController = rootEngineController;
    }

    public void setNodeAI(List<MultiShipAPI> mShips) {
        for (MultiShipAPI mShip : mShips) {
            if (mShip.getTwigData().getAIType().contentEquals("NO_AI")) {
                mShip.getShip().setShipAI(new No_AI());
                mShip.getShipAI().forceCircumstanceEvaluation();

            } else if (mShip.getTwigData().getAIType().contentEquals("SLAVE_AI")) {
                mShip.getShip().setShipAI(new Slave_AI(mShip.getShip(), this.getShip()));
                mShip.getShipAI().forceCircumstanceEvaluation();
            } else {
                //do nothing, use default AI, alternative extend the class and create your own AI and set it as you see fit.
            }
        }
    }

    public FleetSide getFleetSide() {
        if (this.getShip().getOriginalOwner() == 100 || this.getShip().getOriginalOwner() == 0) {
            return FleetSide.PLAYER;

        } else {
            return FleetSide.ENEMY;
        }
    }

    public static class RootEngine implements ShipEngineControllerAPI {

        ShipAPI root;

        public RootEngine(ShipAPI rootShip) {
            this.root = rootShip;
        }

        @Override
        public boolean isAccelerating() {
            return root.getEngineController().isAccelerating();
        }

        @Override
        public boolean isAcceleratingBackwards() {
            return root.getEngineController().isAcceleratingBackwards();
        }

        @Override
        public boolean isDecelerating() {
            return root.getEngineController().isDecelerating();
        }

        @Override
        public boolean isTurningLeft() {
            return root.getEngineController().isTurningLeft();
        }

        @Override
        public boolean isTurningRight() {
            return root.getEngineController().isTurningRight();
        }

        @Override
        public boolean isStrafingLeft() {
            return root.getEngineController().isStrafingLeft();
        }

        @Override
        public boolean isStrafingRight() {
            return root.getEngineController().isStrafingRight();
        }

        @Override
        public List<ShipEngineAPI> getShipEngines() {
            return ((RootShipAPI) root).getAllNodeShipEngines();
        }

        @Override
        public void fadeToOtherColor(Object key, Color other, Color contrailColor, float effectLevel, float maxBlend) {
            root.getEngineController().fadeToOtherColor(key, other, contrailColor, effectLevel, maxBlend);
        }

        @Override
        public void extendFlame(Object key, float extendLengthFraction, float extendWidthFraction, float extendGlowFraction) {
            root.getEngineController().extendFlame(key, extendLengthFraction, extendWidthFraction, extendGlowFraction);
        }

        @Override
        public void forceFlameout() {
            root.getEngineController().forceFlameout();
        }

        @Override
        public void forceFlameout(boolean suppressFloaty) {
            root.getEngineController().forceFlameout(suppressFloaty);
        }

        @Override
        public float getMaxSpeedWithoutBoost() {
            return root.getEngineController().getMaxSpeedWithoutBoost();
        }

        @Override
        public float computeDisabledFraction() {
            return root.getEngineController().computeDisabledFraction();
        }

        @Override
        public float getFlameoutFraction() {
            return root.getEngineController().getFlameoutFraction();
        }

        @Override
        public void computeEffectiveStats(boolean forceShowFloaty) {
            root.getEngineController().computeEffectiveStats(forceShowFloaty);
        }

        @Override
        public boolean isFlamedOut() {
            return root.getEngineController().isFlamedOut();
        }

        @Override
        public boolean isDisabled() {
            return root.getEngineController().isDisabled();
        }

        @Override
        public boolean isFlamingOut() {
            return root.getEngineController().isFlamingOut();
        }

        @Override
        public void setFlameLevel(EngineSlotAPI slot, float level) {
            root.getEngineController().setFlameLevel(slot, level);
        }

        @Override
        public ValueShifterAPI getExtendLengthFraction() {
            return root.getEngineController().getExtendLengthFraction();
        }

        @Override
        public ValueShifterAPI getExtendWidthFraction() {
            return root.getEngineController().getExtendWidthFraction();
        }

        @Override
        public ValueShifterAPI getExtendGlowFraction() {
            return root.getEngineController().getExtendGlowFraction();
        }

    }

    /**
     * @return the rootNodeAPI
     */
    @Override
    public RootShipAPI getRootNode() {
        return this;
    }

}
