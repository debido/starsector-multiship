/*
 * Copyright (c) 2014 Debido.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Debido - initial API and implementation and/or initial documentation
 */
package db.twiglib.core;

import com.fs.starfarer.api.combat.ShipEngineControllerAPI.ShipEngineAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import db.twiglib.TWIGEntity;
import java.util.List;

/**
 *
 * @author Debido
 */
public interface RootShipAPI {

    /**
     *
     * @param segmentData
     * @return
     */
    List<MultiShipAPI> addSegments(List<TWIGEntity> segmentData);

    /**
     * @param nodeList the nodeList to set
     */
    void updateGlobalList(List<MultiShipAPI> nodeList);

    /**
     *
     * @param intervalElapsed
     * @param amount
     */
    void updateNodes(boolean intervalElapsed, float amount);
    
    List<WeaponAPI> getAllNodeWeapons();
    
    List<WeaponAPI> getAllNodeDisabledWeapons();
    
    List<ShipEngineAPI> getAllNodeShipEngines();
    
    List<ShipEngineAPI> getAllNodeDisabledShipEngines();

    public void spawnChildNodes();
    
    
    
}
