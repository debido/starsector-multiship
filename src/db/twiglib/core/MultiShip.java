/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.twiglib.core;

import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import db.twiglib.TWIGEntity;
import db.twiglib.TwigUtils;

/**
 *
 * @author Debido
 */
public class MultiShip extends AbstractMultiShip implements MultiShipAPI {

    /**
     * Default constructor
     */
    public MultiShip() {

    }

    /**
     *
     * The constructor typically used for the root node
     * 
     * @param hostShip
     * @param initData
     */
    public MultiShip(ShipAPI hostShip, TWIGEntity initData) {
        ship = hostShip;
        multiType = ShipType.ROOT;
        Instanced = true;
        RootNode = true;
        ChildHost = true;//just fudge this for the moment, child host does not exist
        this.twigData = initData;
    }

    /**
     *
     * The constructor typically used for sub-nodes
     * 
     * @param hostShip
     * @param subship
     * @param weapon
     * @param mShipInit
     */
    public MultiShip(MultiShip hostShip, ShipAPI subship, WeaponAPI weapon, TWIGEntity mShipInit) {
        this.ship = subship;
        hostNodeAPI = hostShip;
        rootNodeAPI = (RootShipAPI) hostShip;
        multiType = TwigUtils.stringToShipType(mShipInit.getShipType());
        Instanced = true;

        if (multiType == ShipType.UNI || multiType == ShipType.CHILD) {
            this.ChildNode = true;
        }

        this.refitRadius = mShipInit.getRefitRadius();
        this.combatRadius = mShipInit.getCombatRadius();
        this.refitSpriteID = mShipInit.getRefitSpriteID();
        this.combatSpriteID = mShipInit.getCombatSpriteID();
        this.disabledSpriteID = mShipInit.getDisabledSpriteID();

        this.originWeapon = weapon;

        //add weapon disable list and weapon change alpha list
        //if the ejection force is greater than 0, set the eject on death to be true, and set the ejection force to whatever value it is
        if (mShipInit.getEjectionForce() > 0f) {
            this.ejectOnDeath = true;
            this.ejectForce = mShipInit.getEjectionForce();
        }

        this.multiDeathType = TwigUtils.stringToDeathType(mShipInit.getDeathType());

        if (this.multiDeathType == DeathType.DECO_SWAP) {
            subship.getSpriteAPI().setColor(transparent);
        }
        
        this.twigData = mShipInit;
        
        this.onDeathHostWeaponDisable = mShipInit.getOnDeathHostWeaponDisable();
        this.onDeathHostWeaponAlpha = mShipInit.getOnDeathHostWeaponAlpha();

    }


}
