/*
 * Copyright (c) 2014 Debido.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Debido - initial API and implementation and/or initial documentation
 */
package db.twiglib.core;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.input.InputEventAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Debido
 */
public class MultiShipManager extends BaseEveryFrameCombatPlugin {

    private static String GlobalShipRegisterKey = "TWIG_GLOBAL_SHIP_REGISTER_KEY";
    private static String GlobalMultiShipKey = "TWIG_GLOBAL_MULTISHIP_KEY";
    private static IntervalUtil ventTracker = new IntervalUtil(0.3f, 0.7f);

    /**
     *
     * @param rootShip
     * @param childShips
     */
    public static void registerRoot(RootShipAPI rootShip, List<MultiShipAPI> childShips) {
        getGlobalMultiShipAPIs().put(rootShip, childShips);
    }

    /**
     *
     * @param ship
     * @param multiShip
     */
    public static void registerNode(ShipAPI ship, MultiShipAPI multiShip) {
        getGlobalMultiShipAPIRegister().put(ship, multiShip);
    }

    /**
     *
     * @param ship
     * @return
     */
    public static boolean checkIsNode(ShipAPI ship) {
        return getGlobalMultiShipAPIRegister().containsKey(ship);
    }

    /**
     *
     * @param ship
     * @return
     */
    public static MultiShipAPI getNodeFromShip(ShipAPI ship) {
        return getGlobalMultiShipAPIRegister().get(ship);
    }

    /**
     *
     * @param rootShip
     * @return
     */
    public static List<MultiShipAPI> getNodesFromRoot(RootShipAPI rootShip) {
        return new ArrayList<>(getGlobalMultiShipAPIs().get(rootShip));
    }

    /**
     * @return the ventTracker
     */
    public static IntervalUtil getVentTracker() {
        return ventTracker;
    }

    /**
     * @return the GlobalMultiShipAPIRegister
     */
    @SuppressWarnings("unchecked")
    protected static HashMap<ShipAPI, MultiShipAPI> getGlobalMultiShipAPIRegister() {
        return (HashMap<ShipAPI, MultiShipAPI>) Global.getCombatEngine().getCustomData().get(GlobalShipRegisterKey);
    }

    /**
     * @return the GlobalMultiShipAPIs
     */
    @SuppressWarnings("unchecked")
    protected static HashMap<RootShipAPI, List<MultiShipAPI>> getGlobalMultiShipAPIs() {
        
        return (HashMap<RootShipAPI, List<MultiShipAPI>>) Global.getCombatEngine().getCustomData().get(GlobalMultiShipKey);
    }

    public static boolean isRoot(ShipAPI ship) {
        if (checkIsNode(ship)) {
            MultiShipAPI nodeFromShip = getNodeFromShip(ship);
            if (nodeFromShip.isRootNode()) {
                return true;
            }
        }
        
        return false;
    }

    public static boolean isChild(ShipAPI ship) {
        return !isRoot(ship);

    }

    public static List<ShipAPI> getAllNodes() {
        return new ArrayList<>(getGlobalMultiShipAPIRegister().keySet());
    }

    public static List<ShipAPI> getAllRoots() {
        List<ShipAPI> ships = new ArrayList<>();
        List<RootShipAPI> roots = new ArrayList<>(getGlobalMultiShipAPIs().keySet());
        for (RootShipAPI root : roots) {
            ships.add(((MultiShipAPI) root).getShip());
        }
        
        return ships;
    }

    /**
     *
     * @param amount
     * @param events
     */
    @Override
    public void advance(float amount, List<InputEventAPI> events) {

        if (Global.getCombatEngine().isPaused()) {
            return;
        }

        getVentTracker().advance(amount);
        processHosts(amount);
    }

    private void processHosts(float amount) {

        for (RootShipAPI hostShip : getGlobalMultiShipAPIs().keySet()) {
            hostShip.updateNodes(getVentTracker().intervalElapsed(), amount);
        }
    }

    /**
     *
     * @param engine
     */
    @Override
    public void init(CombatEngineAPI engine) {

        HashMap<ShipAPI, MultiShipAPI> GlobalMultiShipAPIRegister = new HashMap<>();
        HashMap<RootShipAPI, List<MultiShipAPI>> GlobalMultiShipAPIs = new HashMap<>();

        Global.getCombatEngine().getCustomData().put(GlobalShipRegisterKey, GlobalMultiShipAPIRegister);
        Global.getCombatEngine().getCustomData().put(GlobalMultiShipKey, GlobalMultiShipAPIs);

    }
}
