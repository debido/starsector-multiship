/*
 * Copyright (c) 2014 Debido.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Debido - initial API and implementation and/or initial documentation
 */
package db.twiglib;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.lwjgl.util.vector.Vector2f;

/**
 *
 * @author Debido
 */
public abstract class AbstractTWIGEntity implements Serializable {
    protected static final long serialVersionUID = 1L;
    protected TWIGEntity root;
    protected String shipID;
    protected String variantID;
    protected String weaponID;
    protected String refitSpriteID;
    protected float refitRadius;
    protected String combatSpriteID;
    protected float combatRadius;
    protected String disabledSpriteID;
    protected float ejectionForce;
    protected String deathType;
    protected String AIType;
    protected String multiType;
    protected Vector2f locOffset = new Vector2f();
    protected float facing;
    protected ArrayList<TWIGEntity> connectedShips = new ArrayList<>();
    protected Map<String, String> additionalProperties = new HashMap<>();
    protected ArrayList<String> onDeathHostWeaponAlpha = new ArrayList<>();
    protected ArrayList<String> onDeathHostWeaponDisable = new ArrayList<>();

    public AbstractTWIGEntity() {
    }

    /**
     *
     * @return The root
     */
    public TWIGEntity getRoot() {
        return this.root;
    }

    /**
     *
     * @param root The root
     */
    public void setRoot(TWIGEntity root) {
        this.root = root;
    }

    /**
     *
     * @return The shipID
     */
    public String getShipID() {
        return shipID;
    }

    /**
     *
     * @param shipID The shipID
     */
    public void setShipID(String shipID) {
        this.shipID = shipID;
    }

    /**
     *
     * @return The variantID
     */
    public String getVariantID() {
        return variantID;
    }

    /**
     *
     * @param variantID The variantID
     */
    public void setVariantID(String variantID) {
        this.variantID = variantID;
    }

    /**
     *
     * @return The weaponID
     */
    public String getWeaponID() {
        return weaponID;
    }

    /**
     *
     * @param weaponID The weaponID
     */
    public void setWeaponID(String weaponID) {
        this.weaponID = weaponID;
    }

    /**
     *
     * @return The refitSpriteID
     */
    public String getRefitSpriteID() {
        return refitSpriteID;
    }

    /**
     *
     * @param refitSpriteID The refitSpriteID
     */
    public void setRefitSpriteID(String refitSpriteID) {
        this.refitSpriteID = refitSpriteID;
    }

    /**
     *
     * @return The refitRadius
     */
    public float getRefitRadius() {
        return refitRadius;
    }

    /**
     *
     * @param refitRadius The refitRadius
     */
    public void setRefitRadius(float refitRadius) {
        this.refitRadius = refitRadius;
    }

    /**
     *
     * @return The combatSpriteID
     */
    public String getCombatSpriteID() {
        return combatSpriteID;
    }

    /**
     *
     * @param combatSpriteID The combatSpriteID
     */
    public void setCombatSpriteID(String combatSpriteID) {
        this.combatSpriteID = combatSpriteID;
    }

    /**
     *
     * @return The combatRadius
     */
    public float getCombatRadius() {
        return combatRadius;
    }

    /**
     *
     * @param combatRadius The combatRadius
     */
    public void setCombatRadius(float combatRadius) {
        this.combatRadius = combatRadius;
    }

    /**
     *
     * @return The disabledSpriteID
     */
    public String getDisabledSpriteID() {
        return disabledSpriteID;
    }

    /**
     *
     * @param disabledSpriteID The disabledSpriteID
     */
    public void setDisabledSpriteID(String disabledSpriteID) {
        this.disabledSpriteID = disabledSpriteID;
    }

    /**
     *
     * @return The ejectionForce
     */
    public float getEjectionForce() {
        return ejectionForce;
    }

    /**
     *
     * @param ejectionForce The ejectionForce
     */
    public void setEjectionForce(float ejectionForce) {
        this.ejectionForce = ejectionForce;
    }

    /**
     *
     * @return The deathType
     */
    public String getDeathType() {
        return deathType;
    }

    /**
     *
     * @param deathType The deathType
     */
    public void setDeathType(String deathType) {
        this.deathType = deathType;
    }

    /**
     *
     * @return The AIType
     */
    public String getAIType() {
        return AIType;
    }

    /**
     *
     * @param AIType The AIType
     */
    public void setAIType(String AIType) {
        this.AIType = AIType;
    }

    /**
     *
     * @return The multiType
     */
    public String getShipType() {
        return multiType;
    }

    /**
     *
     * @param type
     */
    public void setShipType(String type) {
        this.multiType = type;
    }

    /**
     *
     * @param locOffset The locOffset
     */
    public void setLocOffset(Vector2f locOffset) {
        this.locOffset = locOffset;
    }

    /**
     *
     * @return The locOffset
     */
    public Vector2f getLocOffset() {
        return locOffset;
    }

    /**
     *
     * @return The connectedShips
     */
    public ArrayList<TWIGEntity> getConnectedShips() {
        return connectedShips;
    }

    /**
     *
     * @param connectedShips The connectedShips
     */
    public void setConnectedShips(ArrayList<TWIGEntity> connectedShips) {
        this.connectedShips = connectedShips;
    }

    public Map<String, String> getAdditionalProperties() {
        return Collections.unmodifiableMap(this.additionalProperties);
    }

    public void setAdditionalProperty(Map<String, String> map) {
        this.additionalProperties.putAll(map);
    }

    /**
     * @return the facing
     */
    public float getFacing() {
        return facing;
    }

    /**
     * @param facing the facing to set
     */
    public void setFacing(float facing) {
        this.facing = facing;
    }

    /**
     * @return the onDeathHostWeaponAlpha
     */
    public ArrayList<String> getOnDeathHostWeaponAlpha() {
        return new ArrayList<>(onDeathHostWeaponAlpha);
    }

    /**
     * @param onDeathHostWeaponAlpha the onDeathHostWeaponAlpha to set
     */
    public void setOnDeathHostWeaponAlpha(ArrayList<String> onDeathHostWeaponAlpha) {
        this.onDeathHostWeaponAlpha = onDeathHostWeaponAlpha;
    }

    /**
     * @return the onDeathHostWeaponDisable
     */
    public ArrayList<String> getOnDeathHostWeaponDisable() {
        return new ArrayList<>(onDeathHostWeaponDisable);
    }

    /**
     * @param onDeathHostWeaponDisable the onDeathHostWeaponDisable to set
     */
    public void setOnDeathHostWeaponDisable(ArrayList<String> onDeathHostWeaponDisable) {
        this.onDeathHostWeaponDisable = onDeathHostWeaponDisable;
    }
    
}
