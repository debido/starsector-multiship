/*
 * Copyright (c) 2014 Debido.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Debido - initial API and implementation and/or initial documentation
 */
package db.twiglib;

import com.fs.starfarer.api.Global;
import db.twiglib.core.ShipType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Level;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lwjgl.util.vector.ReadableVector2f;
import org.lwjgl.util.vector.Vector2f;

/**
 *
 * @author Debido
 */
public class TwigNodeLibrary {

    private static JSONArray multishipData = new JSONArray();

    private static final String CSV_PATH = "data/config/twig/twig_files.csv";

    private static final Map<String, TWIGEntity> twigTemplateLibrary = new HashMap<>();

    /**
     * @return the multishipData
     */
    public static JSONArray getMultishipData() {
        return multishipData;
    }

    /**
     *
     * @param shipJSON
     * @throws JSONException
     */
    public static void addMultiShipData(JSONObject shipJSON) throws JSONException {
        //multishipData.put(modMultiShips);
//        for (Iterator it = modMultiShips.keys(); it.hasNext();) {
//            String key = (String) it.next();
//            JSONObject value = new JSONObject(modMultiShips.getString(key));
//            multishipData.put(key, value);
//        }

        multishipData.put(shipJSON);

    }

    public static TWIGEntity getRootTwigEntity(String rootEntityID) throws Exception {
        if (twigTemplateLibrary.containsKey(rootEntityID)) {
            return twigTemplateLibrary.get(rootEntityID);
        } else {
            Global.getLogger(TwigNodeLibrary.class).log(Level.FATAL,
                    "Unabled to find rootship template: " + rootEntityID);
            throw new Exception("Unabled to find rootship template: " + rootEntityID);
        }

    }

    public static void loadLibrary() throws IOException, JSONException {
        loadJsons();
        convertToTwigs();

    }

    private static void loadJsons() throws RuntimeException, JSONException, IOException {
        final List<JSONObject> twigFiles = new ArrayList<>();
        final JSONArray csv = Global.getSettings().getMergedSpreadsheetDataForMod(
                "twig_files", CSV_PATH, "ztwiglib");

        final int numFiles = csv.length();
        Global.getLogger(TwigNodeLibrary.class).log(Level.INFO,
                "Found " + numFiles + " twig files");
        for (int x = 0; x < numFiles; x++) {
            JSONObject row = csv.getJSONObject(x);
            String twigFile = row.getString("twig_files");

            try {
                twigFiles.add(Global.getSettings().loadJSON(twigFile));
            } catch (JSONException ex) {
                throw new RuntimeException("Failed to parse version file \""
                        + twigFile + "\"", ex);
            }
        }

        if (!twigFiles.isEmpty()) {
            for (JSONObject twigFile : twigFiles) {
                JSONObject twigTech = twigFile.getJSONObject("TWIGTech");
                JSONArray rootObjects = twigTech.getJSONArray("rootNodes");
                //JSONArray rootObjects = null;
                //rootNodes.toJSONArray(rootObjects);
                for (int count = 0; count < rootObjects.length(); count++) {
                    addMultiShipData((JSONObject) rootObjects.get(count));
                }

                //addMultiShipData(twigFile);
            }
        }

    }

    private static void convertToTwigs() throws JSONException {
        for (int count = 0; count < multishipData.length(); count++) {
            JSONObject data = multishipData.getJSONObject(count);

            TWIGEntity rootTemplate = null;
            try {
                rootTemplate = getRootTemplate(data);
            } catch (Exception ex) {
                Global.getLogger(TwigNodeLibrary.class).log(Level.FATAL, "Unable to create root twig template: ", ex);
            }
            if (rootTemplate != null) {
                twigTemplateLibrary.put(rootTemplate.getShipID(), rootTemplate);
            } else {
                Global.getLogger(TwigNodeLibrary.class).log(Level.FATAL,
                        "Ship " + data.toString() + " not added due to failures in the JSON format");
            }
        }
    }

    private static TWIGEntity getRootTemplate(JSONObject mData) throws JSONException, Exception {

        if (isMissingMandatoryFields(mData)) {
            Global.getLogger(TwigNodeLibrary.class).log(Level.FATAL,
                    "Mandatory fields missing for" + mData.toString());
            return null;
        }

        String shipID = "shipID";
        shipID = mData.getString(shipID);

        String variantID = "variantID";
        variantID = mData.getString(variantID);

        String weaponID = "weaponID";
        if (isValidString(mData, weaponID)) {
            weaponID = mData.getString(weaponID);
        }

        String refitRadius = "refitRadius";
        Long refitRadiusL = 0L;
        if (isValidNumber(mData, refitRadius)) {
            refitRadiusL = mData.getLong(refitRadius);
        }

        String combatRadius = "combatRadius";
        Long combatRadiusL = 0L;

        if (isValidNumber(mData, combatRadius)) {
            combatRadiusL = mData.getLong(combatRadius);
        }

        String refitSpriteID = "refitSpriteID";
        if (isValidString(mData, refitSpriteID)) {
            refitSpriteID = mData.getString(refitSpriteID);
        }

        String combatSpriteID = "combatSpriteID";
        combatSpriteID = mData.getString(combatSpriteID);

        String disabledSpriteID = "disabledSpriteID";

        if (isValidString(mData, disabledSpriteID)) {
            disabledSpriteID = mData.getString(disabledSpriteID);
        }

        String deathType = "deathType";

        if (isValidString(mData, deathType)) {
            deathType = mData.getString(deathType);
        }

        String locOffset = "locOffset";
        Vector2f locOffsetVector = null;

        if (isValidString(mData, deathType)) {
            locOffsetVector = new Vector2f(getLocOffset(mData.getString(locOffset)));
        }
        Map<String, String> additionalProperties = new HashMap<>();
        if (mData.has("additionalProperties") && !mData.isNull("additionalProperties")) {
            additionalProperties.putAll(getMap(mData.getJSONArray("additionalProperties")));
        }

        ShipType shipType = ShipType.ROOT;

        TWIGEntity rootTwig = new TWIGEntity(
                shipID,
                variantID,
                weaponID,
                refitSpriteID,
                refitRadiusL,
                combatSpriteID,
                combatRadiusL,
                disabledSpriteID,
                0f,
                deathType,
                "",
                locOffsetVector,
                additionalProperties);

        ArrayList<TWIGEntity> connectedShips = new ArrayList<>();

        String connectedShipsStr = "connectedShips";
        if (mData.has(connectedShipsStr) && !mData.isNull(connectedShipsStr)) {
            JSONArray entities = null;
            try {
                entities = mData.getJSONArray(connectedShipsStr);
            } catch (JSONException e) {
                throw new JSONException("Unable to get children for " + shipID + " : " + e);
            }

            for (int i = 0; i < entities.length(); i++) {
                if (!entities.isNull(i)) {

                    connectedShips.add(getChildTwigTemplate(entities.getJSONObject(i)));

                }
            }

        } else {
            throw new Exception("Unable to get children for " + shipID);
        }

        rootTwig.setConnectedShips(connectedShips);

        for (TWIGEntity connectedShip : connectedShips) {
            connectedShip.setRoot(rootTwig);
        }

        return rootTwig;

    }

    protected static Map<String, String> getMap(JSONArray jArray) throws JSONException {
        Map<String, String> additionalProperties = new HashMap<>();
        for (int i = 0; i < jArray.length(); i++) {
            if (!jArray.isNull(i)) {
                for (Iterator it = jArray.getJSONObject(i).keys(); it.hasNext();) {
                    String next = (String) it.next();
                    additionalProperties.put(next, jArray.getJSONObject(i).get(next).toString());

                }
            }
        }

        return additionalProperties;
    }

    private static boolean isValidNumber(JSONObject mData, String refitRadius) throws JSONException {
        return !mData.isNull(refitRadius) && isNumber(mData, refitRadius);
    }

    private static ReadableVector2f getLocOffset(String locOffset) {
        String[] locs = locOffset.split(",");
        return new Vector2f(Float.parseFloat(locs[0]), Float.parseFloat(locs[1]));
    }

    private static boolean isMissingMandatoryFields(JSONObject mData) throws JSONException {
        List<String> checkList = Arrays.asList("shipID", "variantID", "refitRadius", "combatRadius", "combatSpriteID");
        for (String check : checkList) {
            if (!mData.has(check) || mData.isNull(check) || mData.getString(check).contentEquals("")) {
                return true;
            }
        }

        return false;
    }

    private static boolean isNumber(JSONObject mData, String key) throws JSONException {
        String chk = mData.getString(key);
        try {
            float chkNmbr = Float.parseFloat(chk);
        } catch (NumberFormatException nfe) {
            Global.getLogger(TwigNodeLibrary.class).log(Level.FATAL,
                    "Field " + key + " is not a number:" + nfe);
            return false;
        }

        return true;
    }

    private static boolean isValidString(JSONObject mData, String key) throws JSONException {
        return mData.has(key) && (!mData.isNull(key) || !mData.getString(key).contentEquals(""));
    }

    private static TWIGEntity getChildTwigTemplate(JSONObject mData) throws JSONException, Exception {

        if (isMissingMandatoryChildFields(mData)) {
            Global.getLogger(TwigNodeLibrary.class).log(Level.FATAL,
                    "Mandatory fields missing for" + mData.toString());
            return null;
        }

        String shipID = "shipID";
        shipID = mData.getString(shipID);

        String variantID = "variantID";
        variantID = mData.getString(variantID);

        String weaponID = "weaponID";
        weaponID = mData.getString(weaponID);

        String refitRadius = "refitRadius";
        Long refitRadiusL = 0L;
        if (isValidNumber(mData, refitRadius)) {
            refitRadiusL = mData.getLong(refitRadius);
        }

        String combatRadius = "combatRadius";
        Long combatRadiusL = 0L;

        if (isValidNumber(mData, combatRadius)) {
            combatRadiusL = mData.getLong(combatRadius);
        }

        String refitSpriteID = "refitSpriteID";
        if (isValidString(mData, refitSpriteID)) {
            refitSpriteID = mData.getString(refitSpriteID);
        }

        String combatSpriteID = "combatSpriteID";
        combatSpriteID = mData.getString(combatSpriteID);

        String disabledSpriteID = "disabledSpriteID";

        if (isValidString(mData, disabledSpriteID)) {
            disabledSpriteID = mData.getString(disabledSpriteID);
        }

        String deathType = "deathType";

        if (isValidString(mData, deathType)) {
            deathType = mData.getString(deathType);
        }

        String locOffset = "locOffset";
        Vector2f locOffsetVector = null;
        String facingStr = "facing";
        Long facingL = 0L;

        if (isValidString(mData, locOffset) && isValidNumber(mData, facingStr)) {
            locOffsetVector = new Vector2f(getLocOffset(mData.getString(locOffset)));
            facingL = mData.getLong(facingStr);
        }

        Map<String, String> additionalProperties = new HashMap<>();
        if (mData.has("additionalProperties") && !mData.isNull("additionalProperties")) {
            additionalProperties.putAll(getMap(mData.getJSONArray("additionalProperties")));
        }

        String multiType = "multiType";

        if (isValidString(mData, multiType)) {
            multiType = mData.getString(multiType);
        }

        String ejectionForceStr = "ejectionForce";
        Long ejectionForceL = 0L;

        if (isValidNumber(mData, ejectionForceStr)) {
            ejectionForceL = mData.getLong(ejectionForceStr);
        }

        String AIType = "AIType";
        if (isValidString(mData, AIType)) {
            AIType = mData.getString(AIType);
        }

        TWIGEntity childTwig = new TWIGEntity(
                shipID,
                variantID,
                weaponID,
                refitSpriteID,
                refitRadiusL,
                combatSpriteID,
                combatRadiusL,
                disabledSpriteID,
                ejectionForceL,
                deathType,
                AIType,
                multiType,
                locOffsetVector,
                facingL,
                additionalProperties);

        ArrayList<TWIGEntity> connectedShips = new ArrayList<>();

        String connectedShipsStr = "connectedShips";
        if (mData.has(connectedShipsStr) && !mData.isNull(connectedShipsStr)) {
            JSONArray entities = mData.getJSONArray(connectedShipsStr);

            for (int i = 0; i < entities.length(); i++) {
                if (!entities.isNull(i)) {
                    for (Iterator it = entities.getJSONObject(i).keys(); it.hasNext();) {
                        String next = (String) it.next();
                        connectedShips.add(getChildTwigTemplate(entities.getJSONObject(i)));

                    }
                }
            }

        }
        childTwig.setConnectedShips(connectedShips);

        String onDeathHostWeaponAlpha = "onDeathHostWeaponAlpha";

        if (isValidString(mData, onDeathHostWeaponAlpha)) {
            childTwig.setOnDeathHostWeaponAlpha(getListFromString(mData, onDeathHostWeaponAlpha));
        }

        String onDeathHostWeaponDisable = "onDeathHostWeaponDisable";

        if (isValidString(mData, onDeathHostWeaponAlpha)) {
            childTwig.setOnDeathHostWeaponDisable(getListFromString(mData, onDeathHostWeaponDisable));
        }

        return childTwig;

    }

    private static boolean isMissingMandatoryChildFields(JSONObject mData) throws JSONException {
        List<String> checkList = Arrays.asList("shipID", "variantID", "combatSpriteID");
        for (String check : checkList) {
            if (!mData.has(check) || mData.isNull(check) || mData.getString(check).contentEquals("")) {
                return true;
            }
        }

        return false;
    }

    private static ArrayList<String> getListFromString(JSONObject mData, String key) throws JSONException {
        String list = mData.getString(key);
        String[] items = list.split(",");
        ArrayList<String> itemArrayList = new ArrayList<>();
        itemArrayList.addAll(Arrays.asList(items));

        return itemArrayList;
    }

    private TwigNodeLibrary() {
    }

}
