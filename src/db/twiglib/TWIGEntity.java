package db.twiglib;

import java.util.Map;
import org.lwjgl.util.vector.Vector2f;

public class TWIGEntity extends AbstractTWIGEntity {
    private static final long serialVersionUID = 1L;


    public TWIGEntity() {

    }

    /*
     This constructor is to be used on roots only, nodes need to have their root added.
     */
    public TWIGEntity(String shipID, String variantID, String weaponID,
            String refitSpriteID, float refitRadius, String combatSpriteID,
            float combatRadius, String disabledSpriteID, float ejectionForce,
            String deathType, String AIType, Vector2f locOffset, Map<String, String> additionalProperties) {
        this.setShipID(shipID);
        this.setVariantID(variantID);
        this.setWeaponID(weaponID);
        this.setRefitSpriteID(refitSpriteID);
        this.setRefitRadius(refitRadius);
        this.setCombatSpriteID(combatSpriteID);
        this.setCombatRadius(refitRadius);
        this.setDisabledSpriteID(disabledSpriteID);
        this.setEjectionForce(ejectionForce);
        this.setDeathType(deathType);
        this.setAIType(AIType);
        this.setShipType(getShipType());
        this.setLocOffset(locOffset);
        this.setConnectedShips(getConnectedShips());
        if (additionalProperties != null) {
            this.setAdditionalProperty(additionalProperties);
        }
    }

    /*
     If its a node it needs to have a root as its owner, if the root is null, that means this is a root itself
     */
    public TWIGEntity(String shipID, String variantID, String weaponID,
            String refitSpriteID, float refitRadius, String combatSpriteID,
            float combatRadius, String disabledSpriteID, float ejectionForce,
            String deathType, String AIType, String multiType,
            Vector2f locOffset, float facing, Map<String, String> additionalProperties) {
        this.setRoot(root);
        this.setShipID(shipID);
        this.setVariantID(variantID);
        this.setWeaponID(weaponID);
        this.setRefitSpriteID(refitSpriteID);
        this.setRefitRadius(refitRadius);
        this.setCombatSpriteID(combatSpriteID);
        this.setCombatRadius(refitRadius);
        this.setDisabledSpriteID(disabledSpriteID);
        this.setEjectionForce(ejectionForce);
        this.setDeathType(deathType);
        this.setAIType(AIType);
        this.setShipType(multiType);
        this.setLocOffset(locOffset);
        this.setFacing(facing);
        if (additionalProperties != null) {
            this.setAdditionalProperty(additionalProperties);
        }

    }

}
