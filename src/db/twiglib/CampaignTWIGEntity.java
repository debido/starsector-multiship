/*
 * Copyright (c) 2014 Debido.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Debido - initial API and implementation and/or initial documentation
 */
package db.twiglib;

import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import org.lwjgl.util.vector.Vector2f;

/**
 *
 * @author Debido
 */
public class CampaignTWIGEntity extends TWIGEntity {

    private static final long serialVersionUID = 1L;

    protected MemoryAPI memory = new TWIGMemory();
    protected HashMap<String, CampaignTWIGEntity> campaignChildren = new HashMap<>();

    /**
     *
     *
     *
     * @param twig
     * @param isRoot
     */
    public CampaignTWIGEntity(TWIGEntity twig, boolean isRoot) {
        this.setRoot(twig.getRoot());
        this.setShipID(twig.getShipID());
        this.setVariantID(twig.getVariantID());
        this.setWeaponID(twig.getWeaponID());
        this.setRefitSpriteID(twig.getRefitSpriteID());
        this.setRefitRadius(twig.getRefitRadius());
        this.setCombatSpriteID(twig.getCombatSpriteID());
        this.setCombatRadius(twig.getCombatRadius());
        this.setDisabledSpriteID(twig.getDisabledSpriteID());
        this.setEjectionForce(twig.getEjectionForce());
        this.setDeathType(twig.getDeathType());
        this.setAIType(twig.getAIType());
        this.setShipType(twig.getShipType());
        this.setLocOffset(twig.getLocOffset());
        this.setFacing(twig.getFacing());
        if (twig.getAdditionalProperties() != null) {
            this.setAdditionalProperty(twig.getAdditionalProperties());
        }
        
        this.setOnDeathHostWeaponAlpha(twig.getOnDeathHostWeaponAlpha());
        this.setOnDeathHostWeaponDisable(twig.getOnDeathHostWeaponDisable());

        this.setConnectedShips(twig.getConnectedShips());

        if (isRoot) {
            for (TWIGEntity childTwig : twig.getConnectedShips()) {
                campaignChildren.put(childTwig.getShipID(), new CampaignTWIGEntity(childTwig, false));
            }
        }
    }

    /**
     * @return the memory
     */
    public MemoryAPI getMemory() {
        return memory;
    }

    /**
     * @return the campaignChildren
     */
    public HashMap<String, CampaignTWIGEntity> getCampaignChildren() {
        return campaignChildren;
    }

    private static class TWIGMemory implements MemoryAPI {

        HashMap<String, Memory> memory = new HashMap<>();

        TWIGMemory() {
        }

        @Override
        public void unset(String key) {
            memory.remove(key);
        }

        @Override
        public void expire(String key, float days) {
            memory.get(key).setExpireTime(days);
        }

        @Override
        public boolean contains(String key) {
            return memory.containsKey(key);
        }

        @Override
        public boolean is(String key, Object value) {
            if (memory.containsKey(key)) {
                if (memory.get(key).getItem() == value) {
                    return true;
                }
            }

            return false;
        }

        @Override
        public boolean is(String key, float value) {
            if (memory.containsKey(key)) {
                if ((float) memory.get(key).getItem() == value) {
                    return true;
                }
            }

            return false;
        }

        @Override
        public boolean is(String key, boolean value) {
            if (memory.containsKey(key)) {
                if ((boolean) memory.get(key).getItem() == value) {
                    return true;
                }
            }

            return false;
        }

        @Override
        public void set(String key, Object value) {
            memory.put(key, new Memory(value));
        }

        @Override
        public void set(String key, Object value, float expire) {
            memory.put(key, new Memory(value, expire));
        }

        @Override
        public Object get(String key) {
            return memory.get(key).getItem();
        }

        @Override
        public String getString(String key) {
            return (String) memory.get(key).getItem();
        }

        @Override
        public float getFloat(String key) {
            return (float) memory.get(key).getItem();
        }

        @Override
        public boolean getBoolean(String key) {
            return (boolean) memory.get(key).getItem();
        }

        @Override
        public boolean between(String key, float min, float max) {
            float val = (float) memory.get(key).getItem();
            return val < min && val > max;
        }

        @Override
        public Collection<String> getKeys() {
            return memory.keySet();
        }

        @Override
        public float getExpire(String key) {
            return memory.get(key).getExpireTime();
        }

        @Override
        public long getLong(String key) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Vector2f getVector2f(String key) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public SectorEntityToken getEntity(String key) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public CampaignFleetAPI getFleet(String key) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void advance(float amount) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void addRequired(String key, String requiredKey) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void removeRequired(String key, String requiredKey) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean isEmpty() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Set<String> getRequired(String key) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void removeAllRequired(String key) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    private static class Memory {

        protected Object item = null;
        protected float expireTime = 0f;
        protected boolean expire = false;

        Memory(Object item) {
            this.item = item;
        }

        Memory(Object item, float days) {
            this.item = item;
            this.expireTime = days;
            this.expire = true;
        }

        /**
         * @return the item
         */
        protected Object getItem() {
            return item;
        }

        /**
         * @param item the item to set
         */
        protected void setItem(Object item) {
            this.item = item;
        }

        /**
         * @return the expireTime
         */
        protected float getExpireTime() {
            return expireTime;
        }

        /**
         * @param expireTime the expireTime to set
         */
        protected void setExpireTime(float expireTime) {
            this.expireTime = expireTime;
            this.expire = true;
        }

        /**
         * @return the expire
         */
        protected boolean isExpire() {
            return expire;
        }

        /**
         * @param expire the expire to set
         */
        protected void setExpire(boolean expire) {
            this.expire = expire;
        }
    }

}
