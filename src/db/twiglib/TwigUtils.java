/*
 * Copyright (c) 2014 Debido.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Debido - initial API and implementation and/or initial documentation
 */
package db.twiglib;

import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.mission.FleetSide;
import db.twiglib.core.DeathType;
import db.twiglib.core.MultiShipAPI;
import db.twiglib.core.MultiShipManager;
import db.twiglib.core.RootShipAPI;
import db.twiglib.core.ShipType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Debido
 */
public class TwigUtils {

    /**
     * Returns a list of child nodes for a given ShipAPI if it is a root ship
     *
     * @param hostShip
     * @return
     */
    public static List<MultiShipAPI> getChildNodes(ShipAPI hostShip) {
        if (!MultiShipManager.checkIsNode(hostShip)) {
            return new ArrayList<>();
        } else {
            RootShipAPI host = (RootShipAPI) MultiShipManager.getNodeFromShip(hostShip);
            return new ArrayList<>(MultiShipManager.getNodesFromRoot(host));
        }
    }

    /**
     *
     * Returns a list of child nodes of a root node as ShipAPI rather than
     * MultiShipAPI
     *
     * @param hostShip
     * @return
     */
    public static List<ShipAPI> getChildNodesAsShips(ShipAPI hostShip) {
        if (!MultiShipManager.checkIsNode(hostShip)) {
            List<ShipAPI> ships = new ArrayList<>();
            return ships;
        } else {
            RootShipAPI host = (RootShipAPI) MultiShipManager.getNodeFromShip(hostShip);
            List<MultiShipAPI> multis = MultiShipManager.getNodesFromRoot(host);
            List<ShipAPI> ships = new ArrayList<>();
            for (MultiShipAPI multi : multis) {
                ships.add(multi);
            }

            return ships;
        }
    }

    /**
     *
     * Checks if it is a MultiShip
     *
     * @param ship
     * @return
     */
    public static boolean isMultiShip(ShipAPI ship) {
        if (MultiShipManager.checkIsNode(ship) && !MultiShipManager.getNodeFromShip(ship).isAlive() && MultiShipManager.getNodeFromShip(ship).isEjectOnDeath()) {
            return false;
        }
        return MultiShipManager.checkIsNode(ship);
    }

    public static boolean isMultiShip(ShipAPI ship, boolean includeEjectOnDeathAndDead) {
        if (MultiShipManager.checkIsNode(ship) && !MultiShipManager.getNodeFromShip(ship).isAlive() && MultiShipManager.getNodeFromShip(ship).isEjectOnDeath() && !includeEjectOnDeathAndDead) {
            return false;
        }
        return MultiShipManager.checkIsNode(ship);

    }

    /**
     *
     * gets the total number of child nodes.
     *
     * @param ship
     * @return
     */
    public static int getNumberOfChildNodes(ShipAPI ship) {
        if (!isMultiShip(ship)) {
            return 0;
        } else {
            List<MultiShipAPI> tmp = getChildNodes(ship);

            return tmp.size();
        }
    }

    public static ShipType stringToShipType(String shipType) {
        switch (shipType) {
            case "CHILD":
                return ShipType.CHILD;
            case "UNI":
                return ShipType.UNI;
            case "ROOT":
                return ShipType.ROOT;
            case "CHILDHOST":
                return ShipType.CHILDHOST;
            default:
                //panic! return child type
                return ShipType.CHILD;
        }
    }

    public static DeathType stringToDeathType(String deathType) {
        if (deathType.contentEquals("SPRITE_SWAP")) {
            return DeathType.SPRITE_SWAP;
        } else if (deathType.contentEquals("DECO_SWAP")) {
            return DeathType.DECO_SWAP;
        } else if (deathType.contentEquals("REMOVE")) {
            return DeathType.REMOVE;
        } else {
            //panic! return child type
            return DeathType.DEFAULT;
        }
    }

    public static ShipAPI getRoot(ShipAPI ship) {
        if (isMultiShip(ship)) {
            MultiShipAPI nodeFromShip = MultiShipManager.getNodeFromShip(ship);
            if (!nodeFromShip.isAlive() && nodeFromShip.isEjectOnDeath()) {
                return ship;
            }
            ShipAPI rootShip = ((MultiShipAPI) nodeFromShip.getRootNode()).getShip();
            return rootShip;
        } else {
            return ship;
        }
    }

    public static ShipAPI getRoot(ShipAPI ship, boolean includeSeperatedHulk) {
        if (isMultiShip(ship)) {
            MultiShipAPI nodeFromShip = MultiShipManager.getNodeFromShip(ship);
            if (!includeSeperatedHulk && !nodeFromShip.isAlive() && nodeFromShip.isEjectOnDeath()) {
                return ship;
            }
            ShipAPI rootShip = ((MultiShipAPI) nodeFromShip.getRootNode()).getShip();
            return rootShip;
        } else {
            return ship;
        }

    }

    /**
     * - By request of Dark Revenant it removes the twig ship specified by
     * ShipAPI from the Collection if the ShipAPI is any part of the twig ship
     * it will remove the entire twig ship, including root
     *
     * @param ship
     * @param col
     *
     */
    public static void filterConnections(ShipAPI ship, Collection<ShipAPI> col) {
        List<ShipAPI> connectedShips = new ArrayList<>();
        if (!isMultiShip(ship)) {
            return;
        } else {
            MultiShipAPI nodeFromShip = MultiShipManager.getNodeFromShip(ship);
            RootShipAPI rootNode = nodeFromShip.getRootNode();
            List<MultiShipAPI> nodesFromRoot = MultiShipManager.getNodesFromRoot(rootNode);

            for (MultiShipAPI node : nodesFromRoot) {
                connectedShips.add(node.getShip());

            }

            connectedShips.add(((MultiShipAPI) rootNode).getShip());

        }

        for (Iterator<ShipAPI> it = col.iterator(); it.hasNext();) {
            ShipAPI filteredShip = it.next();

            if (connectedShips.contains(filteredShip)) {
                it.remove();
            }
        }

    }

    public static FleetSide getFleetSide(ShipAPI ship) {
        if (ship.getOriginalOwner() == 100 || ship.getOriginalOwner() == 0) {
            return FleetSide.PLAYER;

        } else {
            return FleetSide.ENEMY;
        }
    }

    public static boolean isRoot(ShipAPI ship) {
        return MultiShipManager.isRoot(ship);
    }

    public static boolean isChild(ShipAPI ship) {
        return !MultiShipManager.isRoot(ship);
    }

    private TwigUtils() {
    }

}
