/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.data.scripts;

import com.fs.starfarer.api.BaseModPlugin;
import db.twiglib.TwigNodeLibrary;

/**
 *
 * @author Debido
 */
public class MultishipModPlugin extends BaseModPlugin {

//    private static JSONObject multishipData = new JSONObject();
//    private static JSONObject multishipLoader = null;
//
//    private static final String CSV_PATH = "data/config/twig/twig_files.csv";
//
//    /**
//     * @return the multishipData
//     */
//    public static JSONObject getMultishipData() {
//        return multishipData;
//    }
//
//    /**
//     *
//     * @param modMultiShips
//     * @throws JSONException
//     */
//    public static void addMultiShipData(JSONObject modMultiShips) throws JSONException {
//        for (Iterator it = modMultiShips.keys(); it.hasNext();) {
//            String key = (String) it.next();
//            JSONObject value = new JSONObject(modMultiShips.getString(key));
//            multishipData.put(key, value);
//        }
//
//    }

    @Override
    public void onApplicationLoad() throws Exception {
        TwigNodeLibrary.loadLibrary();
        
//        final List<JSONObject> twigFiles = new ArrayList<>();
//        final JSONArray csv = Global.getSettings().getMergedSpreadsheetDataForMod(
//                "twig_files", CSV_PATH, "ztwiglib");
//
//        final int numMods = csv.length();
//        Global.getLogger(MultishipModPlugin.class).log(Level.INFO,
//                "Found " + numMods + " mods with version info");
//        for (int x = 0; x < numMods; x++) {
//            JSONObject row = csv.getJSONObject(x);
//            String twigFile = row.getString("twig_files");
//
//            try {
//                twigFiles.add(Global.getSettings().loadJSON(twigFile));
//            } catch (JSONException ex) {
//                throw new RuntimeException("Failed to parse version file \""
//                        + twigFile + "\"", ex);
//            }
//        }
//
//        if (!twigFiles.isEmpty()) {
//            for (JSONObject twigFile : twigFiles) {
//                JSONArray rootObjects = null;
//                twigFile.toJSONArray(rootObjects);
//                for (int count = 0; count < rootObjects.length(); count++) {
//                    addMultiShipData((JSONObject) rootObjects.get(count));
//                }
//
//                //addMultiShipData(twigFile);
//            }
//        }
    }

}
