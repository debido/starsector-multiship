/*
 * Original code by Xenoargh, modified by Debido
 */
package db.data.scripts.shipai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.ShipAIPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.plugins.ShipThreatPlugin;
import java.util.Collections;
import java.util.List;
import org.lazywizard.lazylib.CollectionUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

/**
 *
 * @author Debido
 */
public class Slave_AI implements ShipAIPlugin {

    private final ShipwideAIFlags flags;
    
    // Our ship object
    private final ShipAPI ship;
    private final ShipAPI hostShip;

    // Our current target (can be null)
    private ShipAPI target = null;

    private final IntervalUtil checkInterval = new IntervalUtil(0.05f, 0.15f);
    //longInterval causes a new evalutation of targets regardless of anything else, to prevent AIs getting stuck stupidly.  
    //Warning; don't run this too often, otherwise the AI won't even aim the guns properly.
    private final IntervalUtil longInterval = new IntervalUtil(2.5f, 5.0f);
    private boolean doneOnce = false;

    public Slave_AI(ShipAPI ship, ShipAPI hostShip) {
        this.flags = new ShipwideAIFlags();
        this.flags.setFlag(ShipwideAIFlags.AIFlags.PURSUING);
        this.ship = ship;
        this.hostShip = hostShip;
    }
    @Override
    public ShipwideAIFlags getAIFlags() {
        return flags;
    }

    @Override
    public void setDoNotFireDelay(float amount) {
        //intentionally blank
    }

    @Override
    public void forceCircumstanceEvaluation() {
        //intentionally blank
    }

    //Main logic
    @Override
    public void advance(float amount) {
        //LOGIC CHECKS
        if (Global.getCombatEngine() == null) {
            return;
        }
        if (Global.getCombatEngine().isPaused()) {
            return;
        }

        //One-time setup for autofire code.
        if (doneOnce == false) {
            //WEAPON CONTROL
            //If weapon groups aren't on autofire, put them on autofire
            if (ship.getVariant().getWeaponGroups().size() > 0) {
                if (!ship.getVariant().getWeaponGroups().get(0).isAutofireOnByDefault()) {
                    ship.getVariant().getWeaponGroups().get(0).setAutofireOnByDefault(true);
                }
            }
            if (ship.getVariant().getWeaponGroups().size() > 1) {
                if (!ship.getVariant().getWeaponGroups().get(1).isAutofireOnByDefault()) {
                    ship.getVariant().getWeaponGroups().get(1).setAutofireOnByDefault(true);
                }
            }
            if (ship.getVariant().getWeaponGroups().size() > 2) {
                if (!ship.getVariant().getWeaponGroups().get(2).isAutofireOnByDefault()) {
                    ship.getVariant().getWeaponGroups().get(2).setAutofireOnByDefault(true);
                }
            }
            if (ship.getVariant().getWeaponGroups().size() > 3) {
                if (!ship.getVariant().getWeaponGroups().get(3).isAutofireOnByDefault()) {
                    ship.getVariant().getWeaponGroups().get(3).setAutofireOnByDefault(true);
                }
            }
            if (ship.getVariant().getWeaponGroups().size() > 4) {
                if (!ship.getVariant().getWeaponGroups().get(4).isAutofireOnByDefault()) {
                    ship.getVariant().getWeaponGroups().get(4).setAutofireOnByDefault(true);
                }
            }
            doneOnce = true;
        }

        if (hostShip.isHoldFire()) {
            ship.setHoldFireOneFrame(true);
        }

        //If the engine's not paused, tick tock
        checkInterval.advance(amount);
        longInterval.advance(amount);

        //If enough time has passed, re-evaluate our best target, reset our weapon states, and randomize the interval.
        if (null != hostShip.getShipTarget()) {
            ship.setShipTarget(hostShip.getShipTarget());
        } else if (longInterval.intervalElapsed()) {
            //Refresh the target
            target = findBestTarget(this.ship);
            longInterval.randomize();

        }

    //MAIN AI LOGIC
        //This code governs the ship's behavior and outputs boolean states that...
        //...are used in the continual control state code.
        //By using an IntervalUtil here, we can do practical load-balancing at the cost of accuracy.
        if (checkInterval.intervalElapsed()) {
            //FLUX CONTROL
            //If Flux is greater than 75% of max, Vent if possible.
            if ((ship.getFluxTracker().getHardFlux() >= ship.getFluxTracker().getMaxFlux() * 0.75f)
                    || (ship.getFluxTracker().getCurrFlux() >= ship.getFluxTracker().getMaxFlux() * 0.9f)) {
                ship.giveCommand(ShipCommand.VENT_FLUX, null, 0);
            }

        //SHIELD CONTROL
            //If shields are down, raise them.
            //Also points the shields at the best possible target, and lowers them if it's going to overload if hit
            if (ship.getShield() != null) {
                if (ship.getShield().isOff()) {
                    ship.giveCommand(ShipCommand.TOGGLE_SHIELD_OR_PHASE_CLOAK, null, 0);
                } else {
                    ShipAPI nearestEnemy = ShipThreatPlugin.getClosestThreat(ship);
                    float possDamage = 0f;
                    List<DamagingProjectileAPI> nearbyProjectiles = CombatUtils.getProjectilesWithinRange(ship.getLocation(), 200f + ship.getCollisionRadius());

                    for (DamagingProjectileAPI shot : nearbyProjectiles) {

                        if (shot.getOwner() != ship.getOwner()) {
                            possDamage += (shot.getDamageAmount() + shot.getEmpAmount()) * ship.getMutableStats().getShieldDamageTakenMult().getModifiedValue();
                        }
                    }
                    if (possDamage + ship.getFluxTracker().getHardFlux() >= ship.getFluxTracker().getMaxFlux() && possDamage < 2000f) {
                        ship.giveCommand(ShipCommand.TOGGLE_SHIELD_OR_PHASE_CLOAK, null, 0);
                    }

                    if (nearestEnemy != null && nearestEnemy.isAlive() && !nearestEnemy.isHulk()) {
                        Vector2f loc = nearestEnemy.getLocation();
                        ship.getMouseTarget().set(loc.getX(), loc.getY());
                    } else if (target != null) {
                        ship.getMouseTarget().set(target.getLocation().getX(), target.getLocation().getY());
                    }

                }
            }

        //UPDATE TARGETS
            //If we don't have a target, find one.   
            //If our target is dead, removed from the sim or has switched sides, find a new target.
            if (target == null // unset 
                    || target.isHulk()
                    || (ship.getOwner() == target.getOwner()) // friendly
                    || !Global.getCombatEngine().isEntityInPlay(target) // completely removed
                    ) {
                target = null;
                target = findBestTarget(ship);
                if (target != null) {
                    ship.setShipTarget(target);
                } else {
                    ship.setShipTarget(null);
                }
            }

        }
        //To help avoid potential bugs with flags
        this.flags.advance(amount);
    }

    @Override
    public boolean needsRefit() {
        return false;
    }

    //This is our sorter, that finds the closest ShipAPI that is not a Drone.
    private ShipAPI findBestTarget(ShipAPI theShip) {
        //Get our nearby enemies and sort them by distance.

        if (hostShip.getShipTarget() != null) {
            return hostShip.getShipTarget();
        }

        List<ShipAPI> nearbyEnemies = ShipThreatPlugin.getThreats(ship);

        if (nearbyEnemies != null && !nearbyEnemies.isEmpty()) {
            Collections.sort(nearbyEnemies, new CollectionUtils.SortEntitiesByDistance(ship.getLocation()));

            for (ShipAPI enemy : nearbyEnemies) {
                //criteria to add to list (you can put anything that will return a true/false here)
                if (enemy.isCapital() && !enemy.isHulk() && Global.getCombatEngine().isEntityInPlay(enemy)) {
                    if (enemy.getPhaseCloak() != null) {
                        if (!enemy.getPhaseCloak().isOn() || !enemy.getPhaseCloak().isActive()) {
                            return enemy;
                        }
                    } else {
                        return enemy;
                    }
                }

                //Enemy Cruisers second
                if (enemy.isCruiser() && !enemy.isHulk() && Global.getCombatEngine().isEntityInPlay(enemy)) {
                    if (enemy.getPhaseCloak() != null) {
                        if (!enemy.getPhaseCloak().isOn() || !enemy.getPhaseCloak().isActive()) {
                            return enemy;
                        }
                    } else {
                        return enemy;
                    }
                }

                //Everything else.
                if (!enemy.isFighter() && !enemy.isDrone() && !enemy.isHulk() && Global.getCombatEngine().isEntityInPlay(enemy)) {
                    return enemy;
                }
            }
        }

        //Still no target?  Cheat!
        nearbyEnemies = Global.getCombatEngine().getShips();
        Collections.sort(nearbyEnemies, new CollectionUtils.SortEntitiesByDistance(ship.getLocation()));
        for (ShipAPI thisEnemy : nearbyEnemies) {
            //Target any non-Hulk enemy ships.  But NOT DRONES OR FIGHTERS.
            if (thisEnemy.getOwner() != ship.getOwner() && !thisEnemy.isFighter() && !thisEnemy.isDrone() && !thisEnemy.isHulk() && Global.getCombatEngine().isEntityInPlay(thisEnemy)) {
                return thisEnemy;
            }
        }
        //Still nothing?  Return a null.  Should never actually happen unless all enemy ships are dead this frame.
        return null;
    }

}
