/*
 * Copyright (c) 2014 Debido.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Debido - initial API and implementation and/or initial documentation
 */
package db.data.scripts.shipai;

import com.fs.starfarer.api.combat.ShipAIPlugin;
import com.fs.starfarer.api.combat.ShipwideAIFlags;

/**
 *
 * @author Debido
 */
public class No_AI implements ShipAIPlugin {
    
    private final ShipwideAIFlags flags;
    
    public No_AI() {
        this.flags = new ShipwideAIFlags();
        this.flags.setFlag(ShipwideAIFlags.AIFlags.PURSUING);
        
    }

    @Override
    public ShipwideAIFlags getAIFlags() {
        return flags;
    }
    
    @Override
    public void setDoNotFireDelay(float amount) {
        //intentionally blank
    }

    @Override
    public void forceCircumstanceEvaluation() {
        //intentionally blank
    }

    @Override
    public void advance(float amount) {
        //to help avoid bugs with AI flags
        this.flags.advance(amount);
    }

    @Override
    public boolean needsRefit() {
        return false;
    }
    
}
